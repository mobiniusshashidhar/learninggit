var app = angular.module("myApp", []);

//provider script example start........

//app.provider("myprovider", function() {
//this.$get = function() {
//console.log("myprovider.$get called");

//  return "myvalue";
//};
//});

//app.provider("myprovider", function() {

//this.value = "my value";

//this.setValue = function (newValue) {
//  this.value = newValue;
//  };
//this.$get = function() {
//console.log("myprovider.$get called");

//return this.value ;
//};

//});
//provider script example end........

//object constructer...
//function myObject() {
//inner object
//this.getValue = function() {
//  return "my object value";
//  };

//}
// 
// factory script example start........
// app.factory("myprovider", function() {
// console.log("Factory function  called");
// return new function() {
// this.getValue = function() {
// return "my object value";
// };
//
// };
// });

//function Person(name) {
//this.names = name;
//}
//app.factory("myprovider", function() {
//  console.log("Factory function  called");
//return new Person("shashi");
//});
//factory script example end........


//factory script example start........
//app.service("myprovider", function() {
//console.log("service function  called");
//this.getValue = function() {
//return "my object value";
//};
//});
//factory script example end........
app.value("myprovider", "myvalue");
//OR
//app.constant("myprovider","myvalue");

app.controller("myctrl", function(myprovider) {
  console.log("myprovider:" + myprovider);
});

app.controller("myctrl2", function(myprovider) {
  console.log("myprovider:" + myprovider);
});
