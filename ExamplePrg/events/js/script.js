var app = angular.module("myApp", []);

app.controller("myCtrl", function ($scope) {
  $scope.technologies =  [ { name:'C#',likes:0,dislikes:0 },
{ name:'ASP.NET',likes:0,dislikes:0 },
{ name:'SQL', likes:0,dislikes:0 }
];

$scope.incrementLikes = function (tech) {
tech.likes++;
}

$scope.decrementLikes = function (tech) {
tech.dislikes++;
}


});
