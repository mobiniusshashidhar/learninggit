var app = angular.module("myApp",["ngRoute"]);
app.config(function ($routeProvider,$locationProvider) {
$routeProvider
.when("/london", {
  templateUrl: "template/london.html",
  controller: "londoncontroller"
})

.when("/paris", {
  templateUrl: "template/paris.html",
  controller: "pariscontroller"
})

.when("/tokyo", {
  templateUrl: "template/tokyo.html",
  controller: "tokyocontroller"
});
$locationProvider.html5Mode(true);
});

app.controller("londoncontroller",function ($scope) {
  $scope.message = "Welcome to london ";
});

app.controller("pariscontroller",function ($scope) {
  $scope.paris = "Welcome to Paris ";
});

app.controller("tokyocontroller",function ($scope) {
  $scope.tokyo = "Welcome to Tokyo ";
});
