var productModule = angular.module('productApp', []);
productModule.controller('productCtrl', function($scope, ProductService) {
  $scope.product= [];

    $scope.getProducts = function() {
    var productService = new ProductService();
      var response = productService.getProducts();
      response.then( function (rse) {
        if(rse) {
            $scope.product=productService.items;
        } else {

        }
      })
      //productService.getProducts();
      //console.log(response);
        console.log(productService.items);
};
    $scope.getProducts();

  })
  .service('ProductService', function($http,ProductModel) {
    var productService = function() {
      this.items = [];
    };

     productService.prototype.getProducts = function() {
  var self = this;
      return $http({
        method: 'GET',
        url: 'assets/items1.json'
      }).then(function (response) {
        angular.forEach(response.data.items, function (item) {
        self.items.push(new ProductModel(item));

    });
        return true;
        // this callback will be called asynchronously
        // when the response is available
      }, function (response) {
        console.log(response.data);
        return false;
        // called asynchronously if an error occurs
        // or server returns response with an error status.
      });
    };
    return productService;

  }) .factory('ProductModel',function() {
     	return function(data){
  			this.id =   data.id ? data.id : 0;
  			this.name = data.name ? data.name: "";
        this.price = data.price ? data.price: 0;
  			this.count = data.count ? data.count : 0;
        this.available = data.available ? data.available : false;
  		};
   });
