var app = angular.module("myApp", []);
app.controller("myCtrl", function($scope) {

  $scope.employee = [{
    name: "Shashi",
    Salary: "18k",
    age: 22
  }, {
    name: "Raju",
    Salary: "23k",
    age: 28
  }, {
    name: "Lokesh",
    Salary: "14k",
    age: 19
  }, {
    name: "Ramu",
    Salary: "19k",
    age: 21
  }];

$scope.employeeView = "employeeTable.html";

});
