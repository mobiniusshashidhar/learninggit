
  var myapp = angular.module('myapp', ["ui.router"])
  myapp.config(function($stateProvider, $urlRouterProvider) {

    // For any unmatched url, send to /route1
    $urlRouterProvider.otherwise("/Home")

    $stateProvider

      .state('Home', {
      url: "/Home",
      templateUrl: "Home.html"
    })

    .state('About', {
      url: "/About",
      templateUrl: "About.html"
    })


    // .state('route1.list', {
    //     url: "/list",
    //     templateUrl: "route1.list.html",
    //     controller: function($scope){
    //       $scope.items = ["A", "List", "Of", "Items"];
    //     }
    // }) for beginners

    // .state('route2', {
    //     url: "/route2",
    //     templateUrl: "route2.html"
    //   })
    //   .state('route2.list', {
    //     url: "/list",
    //     templateUrl: "route2.list.html",
    //     controller: function($scope) {
    //       $scope.things = ["A", "Set", "Of", "Things"];
    //     }
    //   })

  })
