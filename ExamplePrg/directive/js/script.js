angular.module('myApp', [])

.controller('MyCtrl', function($scope) {
  $scope.people = [
    {
      name: 'Mike',
      age: 20
    },
    {
      name: 'Peter S',
      age: 22
    },
      {
      name: 'Shashi S',
      age: 12
    }
  ];
})

.directive('myElement', function () {
  return {
    scope: {
      item: '=myElement'
    },

    //template: '<td>{{ item.name }}</td><td>{{ item.age }}</td>'
    templateUrl: 'tablevalue.html'

  };
})

;
