// declare the calculator-module
var calculatorModule = angular.module('calculatorApp', []);

// Add the calculator-controller to module
calculatorModule.controller('calculatorController', function($scope, CalculatorService) {

    $scope.equation = "";
    $scope.equationSubmitted = "";

    $scope.updateOutput = function(value) {
      if ($scope.equationSubmitted != "") {
        $scope.equation = value;
        $scope.equationSubmitted = "";
      } else {
        $scope.equation += value;
      }
      $('#updateOutput').append('<div style="float:left;text-align: justify;text-justify: inter-word;"> $scope.equation updated to ' + $scope.equation + '</div>');
    };

    $scope.totalcalculate = function() {
      var calculatorService = new CalculatorService();
      var response = calculatorService.calculate($scope.equation);
      if (response) {
        $scope.equationSubmitted = $scope.equation;
        $('#Totalcalculate').append('<div style="float:left;text-align: justify;text-justify: inter-word;"> $scope.equationSubmitted = ' + $scope.equation + '</div>');
        $scope.equation = calculatorService.result;
        $('#Totalcalculate').append('<div style="float:left;text-align: justify;text-justify: inter-word;"> $scope.equation = ' + $scope.equation + '</div>');
      } else {
        $('#Totalcalculate').append('<div style="float:left;text-align: justify;text-justify: inter-word;"> Displaying error excepection ' + calculatorService.exception + '</div>');
        alert(calculatorService.exception);
      }
    };

    $scope.resetClicked = function() {
      $scope.equation = "";
      $scope.equationSubmitted = "";
      $('#ResetClicked').append('<div style="float:left;text-align: justify;text-justify: inter-word;"> reseting the value of $scope.equation  to "' + $scope.equation + '"</div>');
      $('#ResetClicked').append('<div style="float:left;text-align: justify;text-justify: inter-word;"> reseting the value of $scope.equationSubmitted  to "' + $scope.equationSubmitted + '"</div>');

    };

    $scope.clearLast = function() {
      $scope.equation = $scope.equation.substring(0, $scope.equation.length - 1);
      $('#clearLast').append('<div style="float:left;"> $scope.equation updated to "' + $scope.equation + '"</div>');
    };

  })
  .service('CalculatorService', [function() {

    var calculatorService = function() {
      this.result = 0;
      this.exception = "";
    };

    calculatorService.prototype.calculate = function(equation) {
      try {
        this.result = eval(equation);
        $('#serviceCalculate').append('<div style="float:left;text-align: justify;text-justify: inter-word;"> calculate result to "' + this.result + '"</div>');
      } catch (err) {
        this.exception = err;
        $('#serviceCalculate').append('<div style="float:left;text-align: justify;text-justify: inter-word;"> calculate exception to "' + this.exception + '"</div>');
        return false;
      }
      return true;
    };

    return calculatorService;
  }]);
