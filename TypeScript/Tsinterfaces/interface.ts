interface LabelledValue {
    label: string;
    size:number;
}

function printLabel(labelledObj: LabelledValue) {
    return(labelledObj.label+" "+labelledObj.size);
}

let myObj = {size: 10, label: "Size 10 Object"};
document.getElementById("demo").innerHTML = printLabel(myObj);
