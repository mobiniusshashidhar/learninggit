function f(input: boolean) {
    let a = 100;

    if (input) {
        // Still okay to reference 'a'
        let b = a + 1;
        return b;
    }

    // Error: 'b' doesn't exist here
    return b;
}
document.getElementById("demo").innerHTML = f(true);
document.getElementById("demo1").innerHTML = f(false);
