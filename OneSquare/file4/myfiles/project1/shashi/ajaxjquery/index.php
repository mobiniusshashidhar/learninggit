<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Ajax Paginatioin Example</title>
<script type="text/javascript" src="js/jquery-1.11.2.min.js"></script>
<script type="text/javascript">
$(document).ready(function showHint(){

	
	$("#results" ).load( "fetch_pages.php"); //load initial records
	
	//executes code below when user click on pagination links
	$("#results").on( "click", ".pagination a", function (e){
		e.preventDefault();
		//  $(".loading-div").show(); //show loading element
		var page = $(this).attr("data-page"); //get page number from link
		$("#results").load("fetch_pages.php",{"page":page}, function(){ //get content from PHP page
			// $(".loading-div").hide(); //once done, hide loading element
			
			
		});
		
	});
});
</script>
<script>
function showHint() {
	var fd = document.getElementById("frontdate").value ;
	var ed = document.getElementById("enddate").value ;
	var custname = document.getElementById("modalfade").value ;
	
    if (fd.length == 0) { 
        alert('fill the details') ;
        return;
    }  
	  else if(ed.length == 0) { 
        alert('fill the details') ;
        return;
    }  
	else if(custname.length == 0) { 
       alert('fill the details') ;
        return;
    }  
	else {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("myDiv").innerHTML=xmlhttp.responseText;
			
            }
        }
        xmlhttp.open("GET", "fetch_pages.php?fmdate="+fd+"&emd="+ed+"&customerdate="+custname, true);
		
		
        xmlhttp.send();
    }
	//$( "#dialogs" ).dialog( "close" );//closing the ok button
	
}


</script>

<style>
body,td,th {
	<!--font-family: Georgia, "Times New Roman", Times, serif;-->
	<!--color: #333;-->
}
.contents{
	margin: 20px;
	padding: 20px;
	list-style: none;
	text-align:center;


	<!--border: 1px solid #ddd;
	<!--background: #F9F9F9;
	
	border-radius: 5px;-->
}
.contents li{
    margin-bottom: 20px;
}
.loading-div{
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	<!--background: rgba(0, 0, 0, 0.56);-->
	z-index: 999;
	display:none;
}
.loading-div img {
	margin-top: 20%;
	margin-left: 50%;
}

/* Pagination style */
.pagination{margin:0;padding:0;}
.pagination li{
	display: inline;
	padding: 6px 10px 6px 10px;
	border: 1px solid #ddd;
	margin-right: -1px;
	<!--font: 15px/20px Arial, Helvetica, sans-serif;-->
	background: #FFFFFF;
	<!--box-shadow: inset 1px 1px 5px #F4F4F4;-->
}
.pagination li a{
    text-decoration:none;
    <!--color: rgb(89, 141, 235);-->
}
.pagination li.first {
    border-radius: 5px 0px 0px 5px;
}
.pagination li.last {
    border-radius: 0px 5px 5px 0px;
}
.pagination li:hover{
	background: #CFF;
}
.pagination li.active{
	<!--background: #F0F0F0;
	color: #333;-->
}
</style>
</head>
<body>
<!--<div class="loading-div"><img src="ajax-loader.gif" ></div>-->
<div id="results"><!-- content will be loaded here --></div>
<div class="loading-div"></div>

<div id="myDiv">
</div>	
 <div id="dialogs" >
       <form class="form-inline" role="form" method="post">
        <div class="form-group">
           <input type="text" name="fromdate" class="form-control"  placeholder="Form Date" id="frontdate" required>
          </div>
	
	
    <div class="form-group">
    <input type="text" name="enddate" class="form-control"  placeholder="End Date" id="enddate" required>
 </div>
	
      
<div class="form-group">
     <input type="text"  name="cusname" class="form-control" id="modalfade" placeholder="Customer Name" required>
    </div>
  
    
  
   <button type="button" name="ok"  class="btn btn-default" onclick="showHint();"> OK </button>
 
 
 </form>
 </div>

</body>
</html>
