

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Simple Site Designs </title>

<!-- STYLESHEET -->
<link rel="stylesheet" type="text/css" href="http://simplesitedesigns.com/wp-content/themes/showtime/style.css"/>
<link rel="stylesheet" type="text/css" href="http://simplesitedesigns.com/wp-content/themes/showtime/skins/black/black.css"/>
<link rel="stylesheet" type="text/css" href="http://simplesitedesigns.com/wp-content/themes/showtime/js/prettyPhoto/css/prettyPhoto.css"/>
<noscript>
<link rel="stylesheet" type="text/css" href="http://simplesitedesigns.com/wp-content/themes/showtime/style-noscript.css"/>
</noscript>
<!-- SCRIPTS-->
 
<link rel="alternate" type="application/rss+xml" title="Simple Site Designs &raquo; Home Comments Feed" href="http://simplesitedesigns.com/home/feed/" />
		<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"http:\/\/s.w.org\/images\/core\/emoji\/72x72\/","ext":".png","source":{"concatemoji":"http:\/\/simplesitedesigns.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.3.1"}};
			!function(a,b,c){function d(a){var c=b.createElement("canvas"),d=c.getContext&&c.getContext("2d");return d&&d.fillText?(d.textBaseline="top",d.font="600 32px Arial","flag"===a?(d.fillText(String.fromCharCode(55356,56812,55356,56807),0,0),c.toDataURL().length>3e3):(d.fillText(String.fromCharCode(55357,56835),0,0),0!==d.getImageData(16,16,1,1).data[0])):!1}function e(a){var c=b.createElement("script");c.src=a,c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g;c.supports={simple:d("simple"),flag:d("flag")},c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.simple&&c.supports.flag||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
		<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link rel='stylesheet' id='jquery.bxslider-css'  href='//simplesitedesigns.com/wp-content/plugins/testimonials-widget/includes/libraries/bxslider-4/dist/jquery.bxslider.css?ver=4.3.1' type='text/css' media='all' />
<link rel='stylesheet' id='Testimonials_Widget-css'  href='//simplesitedesigns.com/wp-content/plugins/testimonials-widget/assets/css/testimonials-widget.css?ver=4.3.1' type='text/css' media='all' />
<link rel='stylesheet' id='wp-pagenavi-css'  href='http://simplesitedesigns.com/wp-content/themes/showtime/pagenavi-css.css?ver=2.70' type='text/css' media='all' />
<script type='text/javascript' src='http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js?ver=4.3.1'></script>
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://simplesitedesigns.com/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://simplesitedesigns.com/wp-includes/wlwmanifest.xml" /> 
<meta name="generator" content="WordPress 4.3.1" />
<link rel='canonical' href='http://simplesitedesigns.com/' />
<link rel='shortlink' href='http://simplesitedesigns.com/' />
<!-- All in one Favicon 4.3 --><link rel="icon" href="http://simplesitedesigns.com/wp-content/ssdfiles/icon.png" type="image/png"/>
<script type="text/javascript" src="http://simplesitedesigns.com/wp-content/themes/showtime/js/main.js"></script>

<script type="text/javascript" src="http://simplesitedesigns.com/wp-content/themes/showtime/js/cufon.js"></script>

<script type="text/javascript" src="http://simplesitedesigns.com/wp-content/themes/showtime/js/cufon/custom.font.bold.basic.js"></script>
<script type="text/javascript" src="http://simplesitedesigns.com/wp-content/themes/showtime/js/cufon/custom.font.semibold.basic.js"></script>

<script type="text/javascript" src="http://simplesitedesigns.com/wp-content/themes/showtime/js/cufon.config.js"></script>

<script type="text/javascript" src="http://simplesitedesigns.com/wp-content/themes/showtime/js/prettyPhoto/js/jquery.prettyPhoto.js"></script>
<script type="text/javascript">
var autoslide_time =3000;
jQuery(document).ready(function($){$("a[rel^='prettyPhoto']").prettyPhoto();  } );</script>
<link rel="stylesheet" type="text/css" href="http://simplesitedesigns.com/wp-content/themes/showtime/sliders/slider_3d.css"/><script type="text/javascript" src="http://simplesitedesigns.com/wp-content/themes/showtime/sliders/scripts/slider_3d.js"></script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-11155237-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>
                                                                                      
<body>

<div id="header_wrapper">
    <div id="header">
        <div id="logo">
            <a href="http://simplesitedesigns.com"><img src="http://simplesitedesigns.com/wp-content/ssdfiles/logo_wide.png" alt="Simple Site Designs" /></a>
        </div>
        <div class="search_top">
          <form id="searchform" action="http://simplesitedesigns.com" method="get">
            <p><input type="text" name="s" id="s" class="search_top_input" value="Enter keywords.." onfocus="if (this.value == 'Enter keywords..') {this.value = '';}" onblur="if (this.value == '') {this.value = 'Enter keywords..';}"/>
           <input type="submit" value="" name="submit" class="search_top_btn"/></p>
          </form>
        </div><!-- END "div.search" -->
        <div class="clear"></div>
    </div><!-- END "div#header" -->
</div><!-- END "div#header_wrapper" -->
<div id="nav_wrapper">
    <div class="menu-main-container"><ul id="menu-main" class="menu"><li id="menu-item-7" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-5 current_page_item menu-item-7"><a href="http://simplesitedesigns.com/">Home</a></li>
<li id="menu-item-19" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-19"><a href="http://simplesitedesigns.com/design/">Web Development</a>
<ul class="sub-menu">
	<li id="menu-item-663" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-663"><a href="http://simplesitedesigns.com/design/">Web Design</a></li>
	<li id="menu-item-662" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-662"><a href="http://simplesitedesigns.com/intranet/">Intranet Development</a></li>
</ul>
</li>
<li id="menu-item-661" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-661"><a href="http://simplesitedesigns.com/hosting/">Web &#038; Email Hosting</a>
<ul class="sub-menu">
	<li id="menu-item-20" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-20"><a href="http://simplesitedesigns.com/hosting/">Web Hosting</a></li>
	<li id="menu-item-581" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-581"><a href="http://simplesitedesigns.com/email/">Email Hosting</a></li>
</ul>
</li>
<li id="menu-item-21" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-21"><a href="http://simplesitedesigns.com/consultancy/">Consultancy</a></li>
<li id="menu-item-22" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-22"><a href="http://simplesitedesigns.com/topics/portfolio/">Our Portfolio</a></li>
<li id="menu-item-24" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-24"><a href="http://simplesitedesigns.com/contact/">Contact Us</a></li>
</ul></div></div><!-- END "div#nav_wrapper" -->
<div id="intro_wrapper" class="intro_wrapper_3d">
    <div class="intro_home intro_home_3d">
       

    
  <img src="http://simplesitedesigns.com/wp-content/themes/showtime/gfx/intro_text.png" class="intro_text" alt="Show them your work or product!" />
        <p id="slider_3d_desc" class="intro_desc"></p>

        <div class="slider_3d">

        <div class="slider_3d_c"><span style="display:none;" class="1" title="www.evomed.com.au">wp-content/ssdfiles/portfolio/scaled/evomed.jpg</span><img src="http://simplesitedesigns.com/wp-content/themes/showtime/scripts/timthumb.php?src=wp-content/ssdfiles/portfolio/thumbs/evomed.jpg&amp;w=366&amp;h=226&amp;zc=1" alt="EVOMed" /></div><div class="slider_3d_r1"><span style="display:none;" class="1" title="www.customshousehotel.com">wp-content/ssdfiles/portfolio/scaled/customs.jpg</span><img src="http://simplesitedesigns.com/wp-content/themes/showtime/scripts/timthumb.php?src=wp-content/ssdfiles/portfolio/thumbs/customs.jpg&amp;w=366&amp;h=226&amp;zc=1" alt="Customs House" /></div><div class="slider_3d_r2"><span style="display:none;" class="1" title="www.procreate.si">wp-content/ssdfiles/portfolio/scaled/procreate.jpg</span><img src="http://simplesitedesigns.com/wp-content/themes/showtime/scripts/timthumb.php?src=wp-content/ssdfiles/portfolio/thumbs/procreate.jpg&amp;w=366&amp;h=226&amp;zc=1" alt="Procreate" /></div><div class="slider_3d_none"><span style="display:none;" class="1" title="www.blmediasolutions.com.au">wp-content/ssdfiles/portfolio/scaled/blmedia.jpg</span><img src="http://simplesitedesigns.com/wp-content/themes/showtime/scripts/timthumb.php?src=wp-content/ssdfiles/portfolio/thumbs/blmedia.jpg&amp;w=366&amp;h=226&amp;zc=1" alt="BL Media Solutions" /></div><div class="slider_3d_none"><span style="display:none;" class="1" title="www.manfield.net.au">wp-content/ssdfiles/portfolio/scaled/manfield.jpg</span><img src="http://simplesitedesigns.com/wp-content/themes/showtime/scripts/timthumb.php?src=wp-content/ssdfiles/portfolio/thumbs/manfield.jpg&amp;w=366&amp;h=226&amp;zc=1" alt="Manfield Bruny Island" /></div><div class="slider_3d_none"><span style="display:none;" class="1" title="www.mtwellingtonchallenge.com.au">wp-content/ssdfiles/portfolio/scaled/mtwell.jpg</span><img src="http://simplesitedesigns.com/wp-content/themes/showtime/scripts/timthumb.php?src=wp-content/ssdfiles/portfolio/thumbs/mtwell.jpg&amp;w=366&amp;h=226&amp;zc=1" alt=" Mt Wellington Challenge" /></div><div class="slider_3d_none"><span style="display:none;" class="1" title="www.pointtopinnacle.com.au">wp-content/ssdfiles/portfolio/scaled/p2p.jpg</span><img src="http://simplesitedesigns.com/wp-content/themes/showtime/scripts/timthumb.php?src=wp-content/ssdfiles/portfolio/thumbs/p2p.jpg&amp;w=366&amp;h=226&amp;zc=1" alt="Point to Pinnacle" /></div><div class="slider_3d_none"><span style="display:none;" class="1" title="www.hentydesigns.com">wp-content/ssdfiles/portfolio/scaled/henty.jpg</span><img src="http://simplesitedesigns.com/wp-content/themes/showtime/scripts/timthumb.php?src=wp-content/ssdfiles/portfolio/thumbs/henty.jpg&amp;w=366&amp;h=226&amp;zc=1" alt="Henty Designs" /></div><div class="slider_3d_none"><span style="display:none;" class="1" title="www.mojoswimwear.net">wp-content/ssdfiles/portfolio/scaled/mojo.jpg</span><img src="http://simplesitedesigns.com/wp-content/themes/showtime/scripts/timthumb.php?src=wp-content/ssdfiles/portfolio/thumbs/mojo.jpg&amp;w=366&amp;h=226&amp;zc=1" alt="MOJO Swimwear" /></div><div class="slider_3d_none"><span style="display:none;" class="1" title="www.matthewgroom.com.au">wp-content/ssdfiles/portfolio/scaled/groom2.jpg</span><img src="http://simplesitedesigns.com/wp-content/themes/showtime/scripts/timthumb.php?src=wp-content/ssdfiles/portfolio/thumbs/groom2.jpg&amp;w=366&amp;h=226&amp;zc=1" alt="Matthew Groom MP (re-design)" /></div><div class="slider_3d_none"><span style="display:none;" class="1" title="www.schoolstriathlonchallenge.com/">wp-content/ssdfiles/portfolio/scaled/stc.jpg</span><img src="http://simplesitedesigns.com/wp-content/themes/showtime/scripts/timthumb.php?src=wp-content/ssdfiles/portfolio/thumbs/stc.jpg&amp;w=366&amp;h=226&amp;zc=1" alt="Schools Triathlon Challenge" /></div><div class="slider_3d_none"><span style="display:none;" class="1" title="www.buildaquote.com.au">wp-content/ssdfiles/portfolio/scaled/baq.jpg</span><img src="http://simplesitedesigns.com/wp-content/themes/showtime/scripts/timthumb.php?src=wp-content/ssdfiles/portfolio/thumbs/baq.jpg&amp;w=366&amp;h=226&amp;zc=1" alt="BuildaQuote" /></div><div class="slider_3d_none"><span style="display:none;" class="1" title="www.rbsolutions.com.au">wp-content/ssdfiles/portfolio/scaled/rbs.jpg</span><img src="http://simplesitedesigns.com/wp-content/themes/showtime/scripts/timthumb.php?src=wp-content/ssdfiles/portfolio/thumbs/rbs.jpg&amp;w=366&amp;h=226&amp;zc=1" alt="Revelstoke Building Solutions" /></div><div class="slider_3d_none"><span style="display:none;" class="1" title="www.iwest.com.au">wp-content/ssdfiles/portfolio/scaled/iwest.jpg</span><img src="http://simplesitedesigns.com/wp-content/themes/showtime/scripts/timthumb.php?src=wp-content/ssdfiles/portfolio/thumbs/iwest.jpg&amp;w=366&amp;h=226&amp;zc=1" alt="iWest Insurance" /></div><div class="slider_3d_none"><span style="display:none;" class="1" title="www.ecopavilions.com.au">wp-content/ssdfiles/portfolio/scaled/ecopav.jpg</span><img src="http://simplesitedesigns.com/wp-content/themes/showtime/scripts/timthumb.php?src=wp-content/ssdfiles/portfolio/thumbs/ecopav.jpg&amp;w=366&amp;h=226&amp;zc=1" alt="Eco Pavilions" /></div><div class="slider_3d_none"><span style="display:none;" class="1" title="www.matthewgroom.com.au">wp-content/ssdfiles/portfolio/scaled/groom.jpg</span><img src="http://simplesitedesigns.com/wp-content/themes/showtime/scripts/timthumb.php?src=wp-content/ssdfiles/portfolio/thumbs/groom.jpg&amp;w=366&amp;h=226&amp;zc=1" alt="Matthew Groom MP" /></div><div class="slider_3d_none"><span style="display:none;" class="1" title="www.sraccounting.com.au">wp-content/ssdfiles/portfolio/scaled/sraccounting.jpg</span><img src="http://simplesitedesigns.com/wp-content/themes/showtime/scripts/timthumb.php?src=wp-content/ssdfiles/portfolio/thumbs/sraccounting.jpg&amp;w=366&amp;h=226&amp;zc=1" alt="Scanlon Richardson Accounting" /></div><div class="slider_3d_none"><span style="display:none;" class="1" title="www.ellyhoyt.com">wp-content/ssdfiles/portfolio/scaled/elly.jpg</span><img src="http://simplesitedesigns.com/wp-content/themes/showtime/scripts/timthumb.php?src=wp-content/ssdfiles/portfolio/thumbs/elly.jpg&amp;w=366&amp;h=226&amp;zc=1" alt="Elly Hoyt" /></div><div class="slider_3d_l2"><span style="display:none;" class="1" title="www.womenintourismtas.org">wp-content/ssdfiles/portfolio/scaled/witt.jpg</span><img src="http://simplesitedesigns.com/wp-content/themes/showtime/scripts/timthumb.php?src=wp-content/ssdfiles/portfolio/thumbs/witt.jpg&amp;w=366&amp;h=226&amp;zc=1" alt="Women in Tourism Tasmania" /></div><div class="slider_3d_l1"><span style="display:none;" class="1" title="www.bikeworks.com.au">wp-content/ssdfiles/portfolio/scaled/bikeworks.jpg</span><img src="http://simplesitedesigns.com/wp-content/themes/showtime/scripts/timthumb.php?src=wp-content/ssdfiles/portfolio/thumbs/bikeworks.jpg&amp;w=366&amp;h=226&amp;zc=1" alt="Bikeworks Motorcycles" /></div>            
                   
        </div><!-- END "div.slider_3d" -->
    </div><!-- END "div.intro_home" -->
</div><!-- END "div#intro_wrapper" -->

<div id="content_wrapper">
	<div id="content" class="content_3d">
	        <div class="action">
            <h3>Fixed Price Website Development Packages!</h3>
            <a href="design/#pricing" class="action_button"><span>Learn More</span></a>
        </div><!-- END "div.action" -->
          <div class="clear"></div>
        <div class="home_layout_1">
            <div class="col3"><div class="module">	<h3>Web Design</h3>			<div class="textwidget"><p><img src="wp-content/ssdfiles/design.png" alt="image" style="float: left; padding: 10px 10px 10px 5px;">We design and build simple yet creative websites that will make your business stand out. We provide an extremely user-friendly content management system that enables you to personalise your own website. Editing content is as easy as logging in, selecting a page and making changes.</p>
<p><a href="design" class="learnmore">Learn more</a></p>
</div>
		</div><!-- END "div.module" --><div class="module">	<h3>Personal Service</h3>			<div class="textwidget"><p><img src="wp-content/ssdfiles/service.png" alt="image" style="float: left; padding: 10px 10px 10px 5px;">At Simple Site Designs, we take great pride in providing prompt and personal customer service. Your projects are managed by fully qualified, highly experienced developers and we ensure all communications are handled internally by those same people.</p>
</div>
		</div><!-- END "div.module" --></div><div class="col3"><div class="module">	<h3>Web Hosting</h3>			<div class="textwidget"><p><img src="wp-content/ssdfiles/host.png" alt="image" style="float: left; padding: 10px 10px 10px 5px;">Regardless of whether you want to host a simple website, a business extranet, eCommerce application or even if you need a fully managed VPS/cloud computing service, we have a solution for you. Please continue to our web hosting page for detailed information on each of our hosting services.</p>
<p><a href="hosting" class="learnmore">Learn more</a></p>
</div>
		</div><!-- END "div.module" --><div class="module">	<h3>Reliable Infrastructure</h3>			<div class="textwidget"><p><img src="wp-content/ssdfiles/reliable.png" alt="image" style="float: left; padding: 10px 10px 10px 5px;">We utilise the very best data centres across Australia and abroad in Europe and the United States. Every project hosted by us will be run on top quality servers and you can trust that we will look after them day and night. After all, we know that the internet never sleeps.</p>
</div>
		</div><!-- END "div.module" --></div><div class="col3"><div class="module">	<h3>Consultancy </h3>			<div class="textwidget"><p><img src="wp-content/ssdfiles/consult.png" alt="image" style="float: left; padding: 10px 10px 10px 5px;">At Simple Site Designs, we have a wealth of experience working on internet and desktop software solutions from small business, through to large scale corporate enterprises. If you need advice on existing systems or help designing effective solutions for your business needs, we can provide professional consultation.</p>
<p><a href="consult" class="learnmore">Learn more</a></p>
</div>
		</div><!-- END "div.module" --><div class="module">	<h3>Monitoring &#038; Reporting</h3>			<div class="textwidget"><p><img src="wp-content/ssdfiles/report.png" alt="image" style="float: left; padding: 10px 10px 10px 5px;">We personally monitor all of the applications we host to ensure you are getting the best service we can provide at all times. Every website project includes fully automated Google Analytics based reporting configured to deliver scheduled statistics directly to you.</p>
</div>
		</div><!-- END "div.module" --></div>
            <div class="clear"></div>
        </div><!-- END "div.home_layout_1" -->
    </div><!-- END "div#content" -->
</div><!-- END "div#content_wrapper" -->
<div id="footer_top_shadow"></div>
<div id="footer_wrapper">
<div id="footer">
   
    <div class="clear"></div>
</div><!-- END "div#footer" -->
<div id="footer_bottom_wrapper">
        <div id="footer_bottom">   
             <div class="left">© 2012 Simple Site Designs</div>
             <div class="right"><span class="logo_desc">
<a href="http://simplesitedesigns.com/wp-login.php">Log in</a><?php wp_loginout(); ?></span>
                             </div>
        <div class="clear"></div>
        </div><!-- END "div#footer_bottom" -->
    </div><!-- END "div#footer_bottom_wrapper" -->
</div><!-- END "div#footer_wrapper" -->
<script type='text/javascript' src='http://simplesitedesigns.com/wp-includes/js/comment-reply.min.js?ver=4.3.1'></script>
</body>
</html>