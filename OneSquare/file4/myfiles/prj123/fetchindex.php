<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Ajax Paginatioin Example</title>
<!--<script type="text/javascript" src="js/jquery-1.11.2.min.js"></script>-->
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript">
$(document).ready(function() {
        $("#clickOkay").click(function() {
                          var name=$("#frontdate").val();
                          var message=$("#enddate").val();
                       var cusname=$("#modalfade").val();
					    var page = '1' ;
                          $.ajax({
                              type:"post",
                              url:"fetch_pages.php",
                              data:"name="+name+"&message="+message+"&csname="+cusname+"&page="+page,
                              success:function(data){
                                 $("#myDiv").html(data);
                              }
 
                          });
        }); //event handler
		
    // $("#results" ).load( "fetch_pages.php"); //load initial records
	//executes code below when user click on pagination links
	
	$("#myDiv").on( "click", ".pagination a", function (e) {
	e.preventDefault();
     var page = $(this).attr("data-page");
	  var name=$("#frontdate").val();
                          var message=$("#enddate").val();
                       var cusname=$("#modalfade").val();
                          $.ajax({
                              type:"post",
                              url:"fetch_pages.php",
                              data:"name="+name+"&message="+message+"&csname="+cusname+"&page="+page,
                              success:function(data){
                                 $("#myDiv").html(data);
                              }
 
                          });
	});
    }); //document.ready
</script>
<style>
body,td,th {

	
}
.contents{
	
	list-style: none;
	text-align:center;
	
}
.contents table{
    margin-top: -110px;

}


.loading-div{
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	background: rgba(0, 0, 0, 0.56);
	z-index: 999;
	display:none;
}
.loading-div img {
	margin-top: 20%;
	margin-left: 50%;
}

/* Pagination style */
.pagination{margin:0;padding:0;}
.pagination li{
	display: inline;
	padding: 6px 10px 6px 10px;
	border: 1px solid #ddd;
	margin-right: -1px;
	<!--font: 15px/20px Arial, Helvetica, sans-serif;-->
	background: #FFFFFF;
	box-shadow: inset 1px 1px 5px #F4F4F4;
}
.pagination li a{
    text-decoration:none;
    color: rgb(89, 141, 235);
}
.pagination li.first {
    border-radius: 5px 0px 0px 5px;
}
.pagination li.last {
    border-radius: 0px 5px 5px 0px;
}
pagination li:hover{
	background: #CFF;
}
.pagination li.active3{
	background: red;
	color: #333;
	
}
</style>
</head>
<body>



<div id="myDiv">
</div>


  <div id="dialogs" >
       <form class="form-inline" role="form" method="post">
        <div class="form-group">
           <input type="text" name="fromdate" class="form-control"  placeholder="Form Date" id="frontdate" required>
          </div>
	
	
    <div class="form-group">
    <input type="text" name="enddate" class="form-control"  placeholder="End Date" id="enddate" required>
 </div>
	
      
<div class="form-group">
     <input type="text"  name="cusname" class="form-control" id="modalfade" placeholder="Customer Name" required>
    </div>
  
    
  
   <button type="button" name="ok" id="clickOkay"  class="btn btn-default"  > OK </button>
 
 
 </form>
 </div>
 
</body>
</html>
