<?php 
 include('view.php'); //connection to view.php
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap  plugins-->
    <link href="css/css1/bootstrap.min.css" rel="stylesheet"> 
    <link href="css/css1/style.css" rel="stylesheet">


  </head>
  <body>
    <div class="container">
     
      
      <!-- Display messages from database-->
      <div class="col-md-10 col-md-offset-2 content">
                <table class="table table-striped">
                  <thead>
  
				  
                  </thead>
				 
                  <tbody>
                   <?php 
	
		
                     for ($i=0; $i<count($result); $i++) { 
					   
					   echo "<table class='table' >";
					   
					     echo "<tr class='row'>";
				       echo "<th style=' text-align:center;background-color:lightblue;'>" . $pdatee. "</th>";
				      echo "</tr>";
				   
				 echo "<tr class='row'>";
				echo "<th style=' text-align:center;'>" . $pnamee . "</th>";
				   echo "</tr>";
				   
			echo "<tr class='row'>";
				echo "<th style=' text-align:center;'>" . $pdescription . "</th>";
				   echo "</tr>";
				   
				  echo "<tr class='row'>";
				echo "<th style=' text-align:center;'>" . $recieveamt . "</th>";
				   echo "</tr>";
				   
				   echo "<tr class='row'>";
				echo "<th style=' text-align:center;'>" . $paidamount . "</th>";
				   echo "</tr>";
				   
				   echo "<tr class='row'>";
				echo "<th style=' text-align:center;'>" . $closingbal . "</th>";
				   echo "</tr>";

						  
				echo "</table>";  
						  }  
						 
                   ?>
                  </tbody>
                </table>
      </div> 
        <!-- Render pagination based on messages-->
      <div class="pagination-wrap">
                <ul class="pagination">
                  <?PHP

                    if($page_counter == 0){
                        echo "<li><a href=?start2='0' class='active'>0</a></li>";
                       for ($j=1; $j < $paginations; $j++) { 
                          echo "<li><a href=?start2=$j>".$j."</a></li>";
                       }
                    }else{
                        echo "<li><a href=?start2=$previous>Previous</a></li>"; 
                        for ($j=0; $j < $paginations; $j++) {
                         if ($j == $page_counter) {
                            echo "<li><a href=?start2=$j class='active'>".$j."</a></li>";
                         }else{
                            echo "<li><a href=?start2=$j>".$j."</a></li>";
                         } 
                      }if($j != $page_counter+1)
                        echo "<li><a href=?start2=$next>Next</a></li>"; 
                    } 
                    
                  ?>
                </ul>
      </div>
    </div>
  

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="css/js1/bootstrap.min.js"></script>
  </body>
</html>