<!DOCTYPE html>
<html>
<script type="text/javascript" src="jquery-1.4.2.js"></script>
<script type="text/javascript" src="coin-slider.min.js"></script>
<link rel="stylesheet" href="coin-slider-styles.css" type="text/css" />
</head>
<script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#coin-slider').coinslider({ width: 900, navigation: false, delay: 5000 });
  });
</script>
</script>


<body>
<div id='coin-slider'>
  <a href="img01_url" target="_blank">
    <img src='img01.jpg' >
    <span>
      Description for img01
    </span>
  </a>
  ......
  ......
  <a href="imgN_url">
    <img src='imgN.jpg' >
    <span>
      Description for imgN
    </span>
  </a>
</div>
</body>


</html>

