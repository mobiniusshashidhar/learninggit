<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
  
    <!--datapicker plugins-->
 <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  
  <!--SIMPLE SIDE BAR STYLE START------>
 <style>
 
 body,html{
    height: 100%;
  }

  nav.sidebar, .main{
    -webkit-transition: margin 200ms ease-out;
      -moz-transition: margin 200ms ease-out;
      -o-transition: margin 200ms ease-out;
      transition: margin 200ms ease-out;
  }

  .main{
    padding: 10px 10px 0 10px;
  }
 
  @media (min-width: 765px) {
	 
	  
	  .main{
      position: absolute;
      width: calc(100% - 40px); 
      margin-top: 40px;
      float: right;
    }  

    nav.sidebar:hover + .main{
      margin-top: 200px;
    }

    nav.sidebar.navbar.sidebar>.container .navbar-brand, .navbar>.container-fluid .navbar-brand {
      margin-top: 0px;
    }

    nav.sidebar .navbar-brand, nav.sidebar .navbar-header{
      text-align: center;
      width: 100%;
      margin-top: 0px;
    }
    
    nav.sidebar a{
      padding-right: 13px;
	  	
    }

    nav.sidebar .navbar-nav > li:first-child{
      border-top: 1px #e5e5e5 solid;
    }

    nav.sidebar .navbar-nav > li{
      border-bottom: 1px #e5e5e5 solid;
    }

    nav.sidebar .navbar-nav .open .dropdown-menu {
      position: static;
      float: none;
      width: auto;
      margin-top: 0;
      background-color: transparent;
      border: 0;
      -webkit-box-shadow: none;
      box-shadow: none;
    }

    nav.sidebar .navbar-collapse, nav.sidebar .container-fluid{
      padding: 0 0px 0 0px;
    }

    .navbar-inverse .navbar-nav .open .dropdown-menu>li>a {
     
    }	  
	  
	  
nav.sidebar
 {
      width: 100%;
      height: 28%;
      margin-top: -180px;
      float: left;
      margin-bottom:0px;
 

	  

 }
  
  
    nav.sidebar li {
      width: 100%;
	 	
    }

    nav.sidebar:hover{
      margin-top: 0px;
	  float:left;
	  width:100%;
	  
	
	
    }

    .forAnimate{
      opacity: 0;
    }

  }  
  
    @media (min-width: 1330px) {

    .main{
      width: calc(100% - 200px);
      margin-top: 200px;
    }

    nav.sidebar{
      margin-top: 0px;
      float: left;
    }

    nav.sidebar .forAnimate{
      opacity: 1;
    }
	

  }

  nav.sidebar .navbar-nav .open .dropdown-menu>li>a:hover, nav.sidebar .navbar-nav .open .dropdown-menu>li>a:focus {
    color: #CCC;
    background-color: transparent;
  }

  nav:hover .forAnimate{
    opacity: 1;
  }
  section{
    padding-left: 15px;
  }
  

</style>
<!--SIMPLE SIDE BAR STYLE START END------>
<style>

#floatcenter
{
width:100%;
}

#imageheight
{
width:100%;
}
#fromdate
{
width:7em;	
}

#fromdate:hover
{
background-color:#D6FFAD;	
}

#enddate
{
width:7em;	
}
#enddate:hover
{
background-color:#D6FFAD;	
}
#defaulttext:hover
{
background-color:#D6FFAD;	
}

#buutonsize
{
width:7em;
height:2em;	
}

#namelable
{
width:7em;	
}
#namelable:hover
{
background-color:#D6FFAD;	
}
#billsize
{
width:10em;
height:2em;	
}

#sizesdivelement
{
background-color:red;
}
#selectsize
{
width:7em;
}
#leftcontainer
{
float:left;	
}
</style>

    <!----------------------vertical button style start--------------->
  <style>
  
  @media (min-width: 765px) {
		
  .hp-ctn-howItWorks
{
    position:fixed;
    top:50px;
    right: 0px;
    padding:0px;
    margin:0px;
    width: 40px;
    height:40px;
    background:#FF931E;
    z-index:15;
    border-radius: 3px 0px 0px 3px;
		margin-top:-50px;
}

.hp-ctn-howItWorks p
{
    margin: 15px 0px 0px 13px;
}

.hp-ctn-howItWorks p
{
    color: #fff;
    -moz-transform:rotate(-90deg);
    -ms-transform:rotate(-90deg);
    -o-transform:rotate(-90deg);
    -webkit-transform:rotate(-90deg);
}

#printcolor
{
color:white;
}
#printcolor:hover
{
color:black;
}	

	}
  </style>
  
  <!----------------------vertical button style end--------------->

<script>
   $(function() {
   $('#fromdate').datepicker({ dateFormat: 'yy-mm-dd' }).val();
 $('#enddate').datepicker({ dateFormat: 'yy-mm-dd' }).val();
  });
 
</script>

<!-------window print script start------------>
<script>
function printthis()
{
           
            var divContents = $("#divelementprint").html();
            var w = window.open('', '', 'height=600,width=1000');
            w.document.write('<html><head><title>DIV Contents</title>');
            w.document.write('</head><body >');
            w.document.write(divContents);
             w.document.write("<link rel='stylesheet' type='text/css' href='css/WndowPrint.css' />");
            
            w.document.write('</body></html>');
            w.document.close();
           javascript:w.print();
 w.close();
 window.close();
 return false;
}
</script>

<!-------window print script start------------>
</head>
<body>

<!--SIMPLE SIDE BAR  HTML TAGS START------>


<nav  class="navbar navbar-default sidebar" role="navigation" >
<div class="container">

	
<div class="navbar-header">
<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-sidebar-navbar-collapse-1">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>  


</div>


<div class="collapse navbar-collapse " id="bs-sidebar-navbar-collapse-1">
<ul class="nav navbar-nav ">

<div class="container">


<div class="col-md-3" >
<div class="form-horizontal form-group form-group-lg " id="">

<li class="active col-md"><label  class="col-sm-7 control-label" for="">From Date:</label></li> 
<div class="col-sm-5">       
<li class="active col-md-"><input type="text" class="" id="fromdate" placeholder=""></li>
</div>

<li class="active col-md"><label  class="col-sm-7 control-label" for="">End Date:</label></li> 
<div class="col-sm-5">       
<li class="active col-md-"><input type="text" class="" id="enddate" placeholder=""></li></br>
<li class="active col-md"><button type="button"  class=" " id="buutonsize" for="" >View Detail</button></li> 
</div>


</div>
</div>


<div class="col-md-3">
<div class=" form-horizontal form-group form-group-lg" id="">


<li class="active"><label class="col-sm-7 control-label"  for="lg">Name:</label></li>   
<div class="col-sm-5">  
<li class="active" id=""><select id="selectsize">
  <option value="volvo">Customer Name</option>
  <option value="saab">Invoice Number</option>
  <option value="opel">Opel</option>
  <option value="audi">Audi</option>
</select>
</li>

<li class="active col-md"><label class=" " for="lg"></label></li> 
<li class="active col-md-"><input type="text" class="" id="namelable" placeholder=""></li>
<li class="active"><td><button type="button" class="" id="buutonsize" placeholder="" value=""> View Details</button></td></li></br>
</div>

<button type="button" class="" id="billsize" placeholder="" value=""> View Bill By Date And Other Details</button>

</div>
</div>


<div class="col-md-6">
<div  class="" id="divelementprint" style="border:1px solid black;width:100%;height:180px;background-color:red;">

<center>
<h1>output</h1>
</center>
</div>

</div>






</div>

</ul>
</div>



</div>
</nav>
<img class="img-responsive imageside"  id="imageheight" src="payroll/logo/sea.png" alt="" >

<div class="hp-ctn-howItWorks">
<p><a onclick="printthis();" id="printcolor">Print</a></p>
</div>
<!--SIMPLE SIDE BAR  HTML TAGS END------>




</body>
</html>