<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
  
  <script>
   $(function(){

    $(".dropdown-menu li a").hover(function(){

 
      $(".btn:first-child").text($(this).text());
      $(".btn:first-child").val($(this).text());

	});
});

$(document).ready(function(){
    $("#hoverview").hover(function(){
        $(this).css("background-color", "yellow");
        }, function(){
        $(this).css("background-color", "pink");
    });
});
  </script>
</head>
<body>


<div class="btn-group">
    <button class="btn" id="hoverview">Please Select From List</button>
    <button class="btn dropdown-toggle" data-toggle="dropdown">
    <span class="caret"></span>
   </button>
 <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu">
   <li><a tabindex="-1" href="#">Item I</a></li>
   <li><a tabindex="-1" href="#">Item II</a></li>
   <li><a tabindex="-1" href="#">Item III</a></li>
    <li><a tabindex="-1" href="#">Item IV</a></li>
   <li class="divider"></li>
   <li><a tabindex="-1" href="#">Other</a></li>
 </ul>
</div>

</body>
</html>