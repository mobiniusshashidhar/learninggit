<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>


 <style>
#myNavbar
{
background-color:#248F24;

}
 
#colorhr
{
color:white;
font-size:18px;
}
#colorhr:hover
{
color:black;
background-color:#ADEBAD;
}


#colorhome
{
color:white;
font-size:18px;
}
#colorhome:hover
{
color:black;
background-color:#ADEBAD;
}



#colorpayroll
{
color:white;
font-size:18px;
}
#colorpayroll:hover
{
background-color:#ADEBAD;
color:black;
}



#colorpayslip
{
color:white;
font-size:18px;
}
#colorpayslip:hover
{
background-color:#ADEBAD;
color:black;
}


#colorlogout
{
color:white;
font-size:18px;
}

#colorlogout:hover
{
background-color:#ADEBAD;
color:black;
}


p.serif 
{
font-family: "Times New Roman", Times, serif;
}
</style> 
  
</head>
<body>


<div class="body-wrap">
<div class="container_fluid col-md-12 form-horizontal">
<nav id="myNavbar" class="navbar navbar-default" role="navigation">
<!-- Brand and toggle get grouped for better mobile display -->
<div class="container_fluid">
<div class="navbar-header">
<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<a class="navbar-brand" href="#" id="colorhr"><p class="serif">HR</p></a>
</div>
<!-- Collect the nav links, forms, and other content for toggling -->
<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
<ul class="nav navbar-nav">
<li id=""><a  class="navbar-brand" href="#" id="colorhome"><p class="serif">Home</p></a></li>
<li id=""><a  class="navbar-brand" href="#" id="colorpayroll"><p class="serif">Payroll</p></a></li>
<li><a  class="navbar-brand" href="contacts.html" id="colorpayslip"><p class="serif">Payslip</p></a></li>
<!--<li class="dropdown">
<a href="#" data-toggle="dropdown" class="dropdown-toggle">Messages <b class="caret"></b></a>
<ul class="dropdown-menu">
<li><a href="#">Inbox</a></li>
<li><a href="#">Drafts</a></li>
<li><a href="#">Sent Items</a></li>
<li class="divider"></li>
<li><a href="#">Trash</a></li>
</ul>
</li>-->
</ul>
<ul class="nav navbar-nav navbar-right">
<li><a  class="navbar-brand" href="contacts.html" id="colorlogout"><p class="serif">Log Out</p></a></li>
<!--<li class="dropdown">
<a href="#" data-toggle="dropdown" class="dropdown-toggle">logout <b class="caret"></b></a>
<ul class="dropdown-menu">
<li><a href="#">Action</a></li>
<li><a href="#">Another action</a></li>
<li class="divider"></li>
<li><a href="#">Settings</a></li>
</ul>
</li>-->
</ul>
            
</div><!-- /.navbar-collapse -->
        
</div>
</div>
</nav>
</div>



</body>
</html>