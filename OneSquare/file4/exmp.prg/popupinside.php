<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml"> 
    <head> 
        <meta http-equiv="content-type" content="text/html; charset=ISO-8859-1" /> 
        <title>autocomplete with dialog</title>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js" type="text/javascript"></script>
        <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/ui-lightness/jquery-ui.css" type="text/css" rel="stylesheet" />
        <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.js" type="text/javascript"></script>
        <!--
        <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.4/themes/base/jquery-ui.css" type="text/css" rel="stylesheet" />
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.3/jquery.js" type="text/javascript"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.4/jquery-ui.js" type="text/javascript"></script>
        -->

        <!--<script type="text/javascript">
            $(function() {
                $( "#search" ).autocomplete({
                    source: [ "one", "two", "three" ]
                });
                $("#dialog").dialog();
            });
        </script>-->
<script>
$(function()
{
  $("#dialog").dialog();
  $( "#search" ).autocomplete({
    source: [ "one", "two", "three" ]
    });
});
</script>
    </head>
    <body>
        <div id="dialog" class="dialog" title="Testing">
            <div class="ui-widget">
                <label for="search">one, two, three: </label>
                <input id="search" />
            </div>
        </div>

    </body>
</html>