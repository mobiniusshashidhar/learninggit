	function print_today() 
	{
  
		var now = new Date();
		var months = new Array('January','February','March','April','May','June','July','August','September','October','November','December');
		var date = ((now.getDate()<10) ? "0" : "")+ now.getDate();
	function fourdigits(number) 
	{
		return (number < 1000) ? number + 1900 : number;
	}
		var today =  months[now.getMonth()] + " " + date + ", " + (fourdigits(now.getYear()));
		return today;
	}

// from http://www.mediacollege.com/internet/javascript/number/round.html
	function roundNumber(number,decimals) {
		var newString;// The new rounded number
		decimals = Number(decimals);
	if (decimals < 1) 
	{
		newString = (Math.round(number)).toString();
	} else 
	{
		var numString = number.toString();
	if (numString.lastIndexOf(".") == -1) 
	{// If there is no decimal point
		numString += ".";// give it one at the end
	}
		var cutoff = numString.lastIndexOf(".") + decimals;// The point at which to truncate the number
		var d1 = Number(numString.substring(cutoff,cutoff+1));// The value of the last decimal place that we'll end up with
		var d2 = Number(numString.substring(cutoff+1,cutoff+2));// The next decimal, after the last one we want
	if (d2 >= 5) {// Do we need to round up at all? If not, the string will just be truncated
	if (d1 == 9 && cutoff > 0) 
	{// If the last digit is 9, find a new cutoff point
	while (cutoff > 0 && (d1 == 9 || isNaN(d1))) 
	{
	if (d1 != ".") 
	{
		cutoff -= 1;
		d1 = Number(numString.substring(cutoff,cutoff+1));
	} else 
	{
		cutoff -= 1;
	}
        }
	}
		d1 += 1;
	} 
	if (d1 == 10) 
	{
		numString = numString.substring(0, numString.lastIndexOf("."));
		var roundedNum = Number(numString) + 1;
		newString = roundedNum.toString() + '.';
	} 
	else 
	{
		newString = numString.substring(0,cutoff) + d1.toString();
	}
	}
	if (newString.lastIndexOf(".") == -1) 
	{// Do this again, to the new string
		newString += ".";
	}
		var decs = (newString.substring(newString.lastIndexOf(".")+1)).length;
	for(var i=0;i<decimals-decs;i++) newString += "0";
//var newNumber = Number(newString);// make it a number if you like
		return newString; // Output the result to the form field (change for your purposes)
	}

	function update_total() 
	{
		var total = 0;
   		var tax11  = document.getElementById("svtax").value;
  		var tax12  = document.getElementById("sltax").value;
  
	$('.price').each(function(i)
	{
		price = $(this).html().replace("","");  //replace("$","");
		if (!isNaN(price)) total += Number(price);
	});

		total = roundNumber(total,2);
 
	$('#subtotal').html(""+total);//html("$"+total);
	$('#tax1').html((""+total/100*tax11).toFixed(2)); 	 
	$('#tax2').html((""+total/100*tax12).toFixed(2)); 	 
	$('.total').html(""+total);  //html("$"+total);
  
 	var x = document.getElementById("subtotal").innerHTML;
 	var y = document.getElementById("tax1").innerHTML; 
 	var z = document.getElementById("tax2").innerHTML;
	
		var discount = document.getElementById('discountId').value ;
	var discountPercentage = document.getElementById('discountPid').value ;
		if(discount != '') {
			var sumDisc = Number(x) - Number(discount);
			var taxOne1 = (""+sumDisc/100*tax11) ;
			var taxTwo1 = (""+sumDisc/100*tax12) ; 
	
			var trimTaxone1 = Number(taxOne1) ;
			var trimTaxtwo1 = Number(taxTwo1) ;
			
			var trimTaxone11 = trimTaxone1.toFixed(2) ;
			var trimTaxtwo21 = trimTaxtwo1.toFixed(2) ;
	
			$('#tax1').html((""+sumDisc/100*tax11)) ; 	 
			$('#tax2').html(""+sumDisc/100*tax12) ;
			
			var sum = sumDisc + Number(trimTaxone1) + Number(trimTaxtwo1) ;
			
			$('#subtotal').html(""+sumDisc);
		} 
		
		if(discountPercentage != '') {
			$('#perdeiscount').val(""+total/100*discountPercentage);
				  var a = document.getElementById('perdeiscount').value ;
				 
				  var tax1 = $('#tax1').val() ;
				  var tax2 = $('#tax2').val() ;
				  
				  var s = Number(x) - Number(a) ;
				  $('#perdeiscount').val(a) ; 
				  $('#discountId').val(a) ;
				var sum = Number(x) - Number(a) + Number(tax1) + Number(tax2) ;  
		}
		
		if(discount == '' && discountPercentage == '') {
			var sum = Number(x) + Number(y) + Number(z) ; 
		}
		
 	// var sum = Number(x) + Number(y) + Number(z);
 	var totalNum = sum.toFixed();
 	$('.total').html(""+totalNum);
 	
  
 
  	update_balance();
	}

	function update_balance() 
	{
  		var due = $(".total").html().replace("","") - $("#paid").val().replace("",""); // replace("$","");
  		due = roundNumber(due,2);
  
  		$('.due').html(""+due); // html("$"+due);
	}

function update_price() { 
  var row = $(this).parents('.item-row');
  var price = row.find('.cost').val().replace("","") * row.find('.qty').val(); //replace("$","")
  price = roundNumber(price,2);//alert(price);
  //row.find('.price').html(price);
  isNaN(price) ? row.find('.price').html("N/A") : row.find('.price').html(price); // .html("$"+price);
  
  update_total();
}

function bind() { 
  $(".cost").blur(update_price);
  $(".qty").blur(update_price);
      $(".dicountClass").blur(update_total);
  $(".dicountPclass").blur(update_total);
}

$(document).ready(function() {
	$('input').click(function(){
		$(this).select();
	});
  
	$("#paid").blur(update_balance);
  
  
   	function textAreaAdjust(o) {
	    o.style.height = "1px";
	    o.style.height = (25+o.scrollHeight)+"px";
	}

   	var index ;
   	index = 1 ;
	$("#addrow").click(function()
  	{
     		// alert(index) ;
     		document.getElementById("mytext").value = index;
    		$(".item-row:last").after('<tr class="item-row"><td class="item-name"><div class="delete-wpr"><input type="text" class="code" id=itemRow'+index+' placeholder="Enter Item Name..." name=items'+index+' onblur="checkItemnames(this);" required><a class="delete" href="javascript:;" title="Remove row">X</a></div></td><td class="description"><textarea placeholder="Enter Descriptions..." name=desc'+index+' id=descid'+index+' class="descItems" onkeyup="textAreaAdjust(this);valid(this);" onblur="valid(this)" style="overflow:hidden" required></textarea></td><td><textarea class="cost" onkeyup="checkInput1(this)" name=cost'+index+' placeholder="0" required></textarea></td><td><textarea class="qty" name=qty'+index+' onkeyup="checkInput(this);checknoOfqty(this);" placeholder="0" required></textarea></td><td><textarea class="price" name=price'+index+' readonly>0</textarea></td><td style="display:none;"><textarea name=chkqty'+index+' id=chkqtyid'+index+'>0</textarea></td></tr>');
    		
    		
    		if ($(".delete").length > 0) $(".delete").show();
   		bind();
   		index++;
  	});
   
  
  bind();
   var indexval =  document.getElementById("mytext").value
  $(".delete").live('click',function(){
    $(this).parents('.item-row').remove();
    update_total();
    if ($(".delete").length < 2) $(".delete").hide();
     indexval = indexval - 1 ;
     document.getElementById("textmy").value = indexval ;
  }) ;
  
  $("#cancel-logo").click(function(){
    $("#logo").removeClass('edit');
  });
  $("#delete-logo").click(function(){
    $("#logo").remove();
  });
  $("#change-logo").click(function(){
    $("#logo").addClass('edit');
    $("#imageloc").val($("#image").attr('src'));
    $("#image").select();
  });
  $("#save-logo").click(function(){
    $("#image").attr('src',$("#imageloc").val());
    $("#logo").removeClass('edit');
  });
  

});