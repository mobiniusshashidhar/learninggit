<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
  
  
<style>
#submitsize
{
width:20%;
}

#a
{
text-align:center;
font-family:"Times New Roman", Times, serif;
color:#000000;
font-size:1.8em;
}
</style>
  
  
</head>
<body>

<div class="container" id="a" >

<h2>FEEL FREE TO SEND A MESSAGE</h2>
<h4>We are happy to assist you with more information on any queries you have. Our counselors will get back to you within a short time. If you need immediate information, please call the respective branch numbers given under Contact information section in the footer.</h4> 

</div>

<div class="container">

<form class="form-horizontal" role="form">
  
<div class="form-group">
<label class="control-label col-sm-2" for="pwd">Name:</label>
<div class="col-sm-6">          
<input type="text" class="form-control" id="pwd" placeholder="Enter Name">
</div>
</div>
  
<div class="form-group">
<label class="control-label col-sm-2" for="email">Email:</label>
<div class="col-sm-6">
<input type="email" class="form-control" id="email" placeholder="Enter email">
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="email">Message:</label>
<div class="col-sm-6">
<textarea class="form-control custom-control" rows="3" style="resize:none"></textarea>     
</div>
</div>


<div class="form-group">
<label class="control-label col-sm-2" for="email">Resume:</label>
<div class="col-sm-6">
<input class="input " type="file" name="resume">
</div>
</div>


<div class="form-group">        
<div class="col-sm-offset-2 col-sm-10">
<button type="submit" class="btn btn-default" id="submitsize"> Submit </button>
</div>
</div>


</form>
</div>

</body>
</html>