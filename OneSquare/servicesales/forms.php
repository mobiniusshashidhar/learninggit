<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
  
    <!--datapicker plugins-->
 <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  
  <!--SIMPLE SIDE BAR STYLE START------>
 <style>
 
 body,html{
    height: 100%;
  }

  nav.sidebar, .main{
    -webkit-transition: margin 200ms ease-out;
      -moz-transition: margin 200ms ease-out;
      -o-transition: margin 200ms ease-out;
      transition: margin 200ms ease-out;
  }

  .main{
    padding: 10px 10px 0 10px;
  }
 
  @media (min-width: 765px) {
	 
	  
	  .main{
      position: absolute;
      width: calc(100% - 40px); 
      margin-top: 40px;
      float: right;
    }  

    nav.sidebar:hover + .main{
      margin-top: 200px;
    }

    nav.sidebar.navbar.sidebar>.container .navbar-brand, .navbar>.container-fluid .navbar-brand {
      margin-top: 0px;
    }

    nav.sidebar .navbar-brand, nav.sidebar .navbar-header{
      text-align: center;
      width: 100%;
      margin-top: 0px;
    }
    
    nav.sidebar a{
      padding-right: 13px;
	  	
    }

    nav.sidebar .navbar-nav > li:first-child{
      border-top: 1px #e5e5e5 solid;
    }

    nav.sidebar .navbar-nav > li{
      border-bottom: 1px #e5e5e5 solid;
    }

    nav.sidebar .navbar-nav .open .dropdown-menu {
      position: static;
      float: none;
      width: auto;
      margin-top: 0;
      background-color: transparent;
      border: 0;
      -webkit-box-shadow: none;
      box-shadow: none;
    }

    nav.sidebar .navbar-collapse, nav.sidebar .container-fluid{
      padding: 0 0px 0 0px;
    }

    .navbar-inverse .navbar-nav .open .dropdown-menu>li>a {
     
    }	  
	  
	  
nav.sidebar
 {
      width: 100%;
      height: 30%;
      margin-top: -180px;
      float: left;
      margin-bottom: 0px; 

	  

 }
  
  
    nav.sidebar li {
      width: 100%;
	 	
    }

    nav.sidebar:hover{
      margin-top: 0px;
	  float:left;
	  width:100%;
	
	
    }

    .forAnimate{
      opacity: 0;
    }

  }  
  
    @media (min-width: 1330px) {

    .main{
      width: calc(100% - 200px);
      margin-left: 200px;
    }

    nav.sidebar{
      margin-left: 0px;
      float: left;
    }

    nav.sidebar .forAnimate{
      opacity: 1;
    }
	

  }

  nav.sidebar .navbar-nav .open .dropdown-menu>li>a:hover, nav.sidebar .navbar-nav .open .dropdown-menu>li>a:focus {
    color: #CCC;
    background-color: transparent;
  }

  nav:hover .forAnimate{
    opacity: 1;
  }
  section{
    padding-left: 15px;
  }
  

</style>
<!--SIMPLE SIDE BAR STYLE START END------>
<style>
#floatcenter
{
width:100%;
}

#imageheight
{
width:100%;
}
#fromdate:hover
{
background-color:#D6FFAD;	
}
#enddate:hover
{
background-color:#D6FFAD;	
}
#defaulttext:hover
{
background-color:#D6FFAD;	
}

</style>



<script>
   $(function() {
   $('#fromdate').datepicker({ dateFormat: 'yy-mm-dd' }).val();
 $('#enddate').datepicker({ dateFormat: 'yy-mm-dd' }).val();
  });
</script>
</head>
<body>

<!--SIMPLE SIDE BAR  HTML TAGS START------>


<nav  class="navbar navbar-default sidebar" role="navigation" >
<div class="container">

	
<div class="navbar-header">
<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-sidebar-navbar-collapse-1">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>  


</div>


<div class="collapse navbar-collapse " id="bs-sidebar-navbar-collapse-1">
<ul class="nav navbar-nav ">

<div class="container">
<div class=" col-md-6 form-horizontal " id="">
<table class=" table table-bordered " id="">

<tbody class="form-group">

<tr>
<li class="active col-md"><td><label class=" " for="">From Date:</label></td></li>        
<li class="active col-md-"><td><input type="text" class="" id="fromdate" placeholder=""></td></li>

</tr>

<tr>
<li class="active col-md"><td><label class=" " for="">End Date:</label></td></li>        
<li class="active col-md-"><td><input type="text" class="" id="enddate" placeholder=""></td></li>
</tr>

<tr>
<li class="active"><td><label class=" " for=""></label></td></li>        
<li class="active"><td><button type="button" class="" id="" placeholder="" value=""> View-By-Dates</button></td></li>
</tr>


</tbody>
</table>
</div>



<div class=" col-md-6 form-horizontal" id="">
<table class=" table table-bordered " id="bordertable">

<tbody class="form-group">

<tr>


<li class="active"><td><label class=" "  for="">Name:</label></td></li>   
     
<li class="active" id="">
<td>
<select>
  <option value="volvo">Customer Name</option>
  <option value="saab">Invoice Number</option>
  <option value="opel">Opel</option>
  <option value="audi">Audi</option>
</select>
</td>
</li>

</tr>

<tr>
<li class="active col-md"><td><label class=" " for=""></label></td></li>        
<li class="active col-md-"><td><input type="text" class="" id="defaulttext" placeholder=""></td></li>

</tr>

<tr>
<li class="active"><td><label class=" " for=""></label></td></li>        
<li class="active"><td><button type="button" class="" id="" placeholder="" value=""> View-By-Details</button></td></li>
</tr>

</tbody>
</table>
</div>


<button type="button" class="" id="floatcenter" placeholder="" value=""> View Bill By Date And Other Details</button>


</div>

</ul>
</div>


</div>
</nav>

<img class="img-responsive imageside"  id="imageheight" src="payroll/logo/sea.png" alt="" >


<!--SIMPLE SIDE BAR  HTML TAGS END------>




</body>
</html>