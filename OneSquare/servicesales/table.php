<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>


<style>
@media only screen and (max-width: 800px) {
    
    /* Force table to not be like tables anymore */
	#no-more-tables1 table, 
	#no-more-tables1 thead, 
	#no-more-tables1 tbody, 
	#no-more-tables1 th, 
	#no-more-tables1 td, 
	#no-more-tables1 tr { 
		display: block; 
	}
 
	/* Hide table headers (but not display: none;, for accessibility) */
	#no-more-tables1 thead tr { 
		position: absolute;
		top: -9999px;
		left: -9999px;
	}
 
	#no-more-tables1 tr { border: 1px solid #ccc; }
 
	#no-more-tables1 td { 
		/* Behave  like a "row" */
		border: none;
		border-bottom: 1px solid #eee; 
		position: relative;
		padding-left: 50%; 
		white-space: normal;
		text-align:left;
	}
 
	#no-more-tables1 td:before { 
		/* Now like a table header */
		position: absolute;
		/* Top/left values mimic padding */
		top: 6px;
		left: 6px;
		width: 45%; 
		padding-right: 10px; 
		white-space: nowrap;
		text-align:left;
		font-weight: bold;
	}
 
	/*
	Label the data
	*/
	#no-more-tables1 td:before { content: attr(data-title1); }
}
</style>
</head>
</body>

<div class="container">
<div class="row">
<div class="col-md-12">


</div>
<div id="no-more-tables1">
<table class="col-md-12 table-bordered cf">
<thead class="cf">
<tr>
        				
<th class="">Amount Paid</th>
<th class="">Balance Due</th>
<th class="">Discount</th>
<th class="">Total</th>
        			
</tr>
</thead>
<tbody>
<tr>
        				
<td data-title1="Amount Paid" class=""><input type="text" id="mBAmountPaid" name=""></td>
<td data-title1="Balance Due" class=""><input type="text" id="mBBalanceDue" name=""></td>
<td data-title1="Discount" class=""><input type="text" id="mBDiscount" name=""></td>
<td data-title1="Total" class=""><input type="text" id="mBTotal" name=""></td>
        				
</tr>
        		
</tbody>
</table>
</div>
</div>

</div>

</body>
</html>