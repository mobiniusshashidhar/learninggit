<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
  
 <!--------------------Add Customer style start--------------------------------> 
<style>
#aCname1
{
border-radius:5px;
background-color:#EBEBD6;
width:6em;
text-align:center;	
}
#aCname
{
border: 0px solid white;	
}
#aCname:hover
{
background-color:#CCFFCC;
}

#aCstate1
{
border-radius:5px;
background-color:#EBEBD6;
width:6em;
text-align:center;	
}

#aCstate
{
border: 0px solid white;	
}
#aCstate:hover
{
background-color:#CCFFCC;
}

#aCPinCode1
{
border-radius:5px;
background-color:#EBEBD6;
width:6em;
text-align:center;	
}

#aCPinCode
{
border: 0px solid white;	
}
#aCPinCode:hover
{
background-color:#CCFFCC;
}

#aCPhoneNo1
{
border-radius:5px;
background-color:#EBEBD6;
width:6em;
text-align:center;	
}

#aCPhoneNo
{
border: 0px solid white;	
}
#aCPhoneNo:hover
{
background-color:#CCFFCC;
}

#aCusemail
{
border-radius:5px;
background-color:#EBEBD6;
width:6em;
text-align:center;	
}

#aCEmail
{
border: 0px solid white;	
}
#aCEmail:hover
{
background-color:#CCFFCC;
}


#aCusTinNo
{
border-radius:5px;
background-color:#EBEBD6;
width:6em;
text-align:center;	
}

#aCTinNo
{
border: 0px solid white;	
}
#aCTinNo:hover
{
background-color:#CCFFCC;
}

#aCusPanNo
{
border-radius:5px;
background-color:#EBEBD6;
width:6em;
text-align:center;	
}

#aCPanNo
{
border: 0px solid white;	
}
#aCPanNo:hover
{
background-color:#CCFFCC;
}


#aCusAddress1
{
border-radius:5px;
background-color:#EBEBD6;
width:6em;
text-align:center;	
}

#aCAddress1
{
border: 0px solid white;	
}
#aCAddress1:hover
{
background-color:#CCFFCC;
}

#aCusAddress2
{
border-radius:5px;
background-color:#EBEBD6;
width:6em;
text-align:center;	
}

#aCAddress2
{
border: 0px solid white;	
}
#aCAddress2:hover
{
background-color:#CCFFCC;
}


#aCusContactName
{
border-radius:5px;
background-color:#EBEBD6;
width:6em;
text-align:center;	
}

#aCContactName
{
border: 0px solid white;
}
#aCContactName:hover
{
background-color:#CCFFCC;
}


#aCusopeningBal
{
border-radius:5px;
background-color:#EBEBD6;
width:6em;
text-align:center;	
}

#aCopeningBal
{
border: 0px solid white;	
}
#aCopeningBal:hover
{
background-color:#CCFFCC;
}

#aCusDueDays
{
border-radius:5px;
background-color:#EBEBD6;
width:7em;
text-align:center;	
}

#aCDueDays
{
width:10em;
border: 0px solid white;
}
#aCDueDays:hover
{
background-color:#CCFFCC;
}

#aCSaveCusButton
{
	width:170px;
margin-left:87px;
color:white;
background-color:#37a69b;	
}
</style>

<!--------------------Add Customer style end-------------------------------->
</head>
<body>

<div class="container">

<!--------------------Add Customer design start-------------------------------->

  <!-- Trigger the modal with a button -->
  <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal123">Add Customer</button>

  <!-- Modal -->
<div class="modal fade" id="myModal123" role="dialog">
<div class="modal-dialog" style="font-size:;width:%;">
<!-- Modal content-->
<div class="modal-content">
<h2 style="text-align:center;">Add New Customer</h2>
</br>

<div class="container">


<label id="aCname1">Name:</label> 
&nbsp&nbsp&nbsp&nbsp;<input type="text" class="col-md-" id="aCname" placeholder="Enter Name">

</br>

<label id="aCstate1">State:</label>    
&nbsp&nbsp&nbsp&nbsp;<input type="text" class="" id="aCstate" placeholder="Enter State">

</br>


<label id="aCPinCode1">Pin Code:</label>     
&nbsp&nbsp&nbsp&nbsp;<input type="text" class="" id="aCPinCode" placeholder="Enter pin-code">

</br>

<label id="aCPhoneNo1">PhoneNo:</label>     
&nbsp&nbsp&nbsp&nbsp;<input type="text" class="" id="aCPhoneNo" placeholder="Enter Phone Number">

</br>

<label id="aCusemail">Email:</label>        
&nbsp&nbsp&nbsp&nbsp;<input type="text" class="" id="aCEmail" placeholder="Enter email">
</br>


<label id="aCusTinNo">Tin No:</label>        
&nbsp&nbsp&nbsp&nbsp;<input type="text" class="" id="aCTinNo" placeholder="Enter Tin Number">
</br>

<label id="aCusPanNo">PAN No:</label>        
&nbsp&nbsp&nbsp&nbsp;<input type="text" class="" id="aCPanNo" placeholder="Enter PAN No">
</br>

<div class="container form-inline">
<label id="aCusAddress1" class="form-group">Address:</label>
<textarea class="form-control" rows="2" id="aCAddress1"  placeholder="Enter Address"></textarea>
<label id="aCusAddress2" class="form-group">Address 2:</label>
<textarea class="form-control" rows="2" id="aCAddress2"  placeholder="Enter Address2"></textarea>
</div>
</br>


<label id="aCusContactName">Contact Name:</label>        
&nbsp&nbsp&nbsp&nbsp;<input type="text" class="" id="aCContactName" placeholder="Enter Contact Name">
</br>

<label id="aCusopeningBal">Opening Balance:</label>        
&nbsp&nbsp&nbsp&nbsp;<input type="text" class="" id="aCopeningBal" placeholder="Opening Balance">
</br>

<div class="container form-inline">
<button type="button" class="form-group"  id="aCSaveCusButton">Save-Customer</button>
&nbsp&nbsp&nbsp&nbsp;<label id="aCusDueDays" class="form-group" >Due Days</label>        
<input type="text" class="" id="aCDueDays" placeholder="Due Days">
</div>
</br>


</div>
     
<div class="modal-footer">
<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>
</div>
      
    </div>
  </div>
<!--------------------Add Customer design end--------------------------------> 
 
</div>

</body>
</html>
