<!DOCTYPE html>
<html lang="en">
<head>
<title>Day Book</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

  
<!------autocomplete plug in------------------>  
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>  


<script>
$(function() {
$('#fromDate').datepicker({ dateFormat: 'yy-mm-dd' }).val();
$('#endDate').datepicker({ dateFormat: 'yy-mm-dd' }).val();
});

</script>
</script>

<style>
#logosise
{
width:150px;
height:65px;
float:left;
} 

</style>


<!---bootstrap navbar menu button style- start------------------->

<style>
#myNavbar
{
background-color:#248F24;

}
 
#colorhr
{
color:white;
font-size:18px;
}
#colorhr:hover
{
color:black;
background-color:#ADEBAD;
}


#colorhome
{
color:white;
font-size:18px;
}
#colorhome:hover
{
color:black;
background-color:#ADEBAD;
}



#colorpayroll
{
color:white;
font-size:18px;
}
#colorpayroll:hover
{
background-color:#ADEBAD;
color:black;
}



#colorpayslip
{
color:white;
font-size:18px;
}
#colorpayslip:hover
{
background-color:#ADEBAD;
color:black;
}


#colorlogout
{
color:white;
font-size:18px;
}

#colorlogout:hover
{
background-color:#ADEBAD;
color:black;
}


p.serif 
{
font-family: "Times New Roman", Times, serif;
}

<!---bootstrap navbar menu button style end-------------------->
</style>

<style>
@media only screen and (max-width: 800px) {
    
    /* Force table to not be like tables anymore */
	#no-more-tables table, 
	#no-more-tables thead, 
	#no-more-tables tbody, 
	#no-more-tables th, 
	#no-more-tables td, 
	#no-more-tables tr { 
		display: block; 
	}
 
	/* Hide table headers (but not display: none;, for accessibility) */
	#no-more-tables thead tr { 
		position: absolute;
		top: -9999px;
		left: -9999px;
	}
 
	#no-more-tables tr { border: 1px solid #ccc; }
 
	#no-more-tables td { 
		/* Behave  like a "row" */
		border: none;
		border-bottom: 1px solid #eee; 
		position: relative;
		padding-left: 50%; 
		white-space: normal;
		text-align:left;
	}
 
	#no-more-tables td:before { 
		/* Now like a table header */
		position: absolute;
		/* Top/left values mimic padding */
		top: 6px;
		left: 6px;
		width: 45%; 
		padding-right: 10px; 
		white-space: nowrap;
		text-align:left;
		font-weight: bold;
	}
 
	/*
	Label the data
	*/
	#no-more-tables td:before { content: attr(data-title); }
}
</style>
<style>
#viewDetail {
float: right;
margin-right: 5em;
width: 10em;

background-color: #248F24;
color: white;
border-radius: 10px;
}

#viewDetail:hover
{
	color:black;
	background-color:#ADEBAD;
}
@media only screen and (max-width: 800px) {
	
	#viewDetail 
	{
float: right;
margin-right: 2em;
width: 7em;
margin-top: -1em;

}

}

#viewBillByAll
{
float: right;
margin-right: 5em;
width: 10em;	
background-color: #248F24;
color: white;
border-radius: 10px;
}

#viewBillByAll:hover
{
color:black;
background-color:#ADEBAD;
}

@media only screen and (max-width: 800px) {
	
#viewBillByAll
	{
float: right;
margin-right: 2em;
width: 10em;
margin-top: -1em;
}

}

#searchSize
{
width:10em;	
height:27px;
border:1px solid white;
}

#dashBox
{
width:10em;	
border:1px solid white;	
}
#dashBox:hover
{
background-color:#CCFFCC;	
}

#viewByName
{
float: right;
margin-right: 5em;
width: 11em;	
background-color: #248F24;
color: white;
border-radius: 10px;

}

#viewByName:hover
{
color:black;
background-color:#ADEBAD;
}

@media only screen and (max-width: 800px) {
	
#searchSize
{
width:9em;	
}

#viewByName
{
float: right;
margin-right: 1em;
width: 8em;	
}
#dashBox
{
width:9em;
	
}
}

#footer
{
float:right;
margin-right:1em;
}	
#footercolor
{
color:#3399CC;
}
#foo1
{
color:black;	
}

#fromDate
{
border:1px solid white;	
width:8em;
}

#fromDate:hover
{
background-color:#CCFFCC;		
}

#endDate
{
border:1px solid white;	
width:8em;
}

#endDate:hover
{
background-color:#CCFFCC;		
}

#simpledash 
{
border:1px solid white;	
width:1em;
}
#simpledash1 
{
border:1px solid white;	
width:1em;
}

#coloramount
{
background-color:#eee;	
text-align:center;
}
.table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th {
border: 1px solid #050505;
}

#FromEndSearch
{
background-color:#eee;	
}

.thumbnail
{
border: 1px solid black;	
width:101%;
}
</style>

<!-------------footer nav menu buttons style  start--------------------->	
<style>
#myNavbars
{
background-color:white;
}	

#Stock
{
color:white;
font-size:18px;
font-family: "Times New Roman", Times, serif;
}
#Stock:hover
{
color:black;
background-color:#ADEBAD;
}


#pettyCash
{
color:white;
font-size:18px;
font-family: "Times New Roman", Times, serif;
}
#pettyCash:hover
{
color:black;
background-color:#ADEBAD;
}

#generalLedger
{
color:white;
font-size:18px;
font-family: "Times New Roman", Times, serif;
}
#generalLedger:hover
{
color:black;
background-color:#ADEBAD;
}

#generalVoucher
{
color:white;
font-size:18px;
font-family: "Times New Roman", Times, serif;
}
#generalVoucher:hover
{
color:black;
background-color:#ADEBAD;
}

#peportSize
{
color:white;
font-size:18px;
font-family: "Times New Roman", Times, serif;
}
#peportSize:hover
{
color:black;
background-color:#ADEBAD;
}

#chequeSize
{
color:white;
font-size:18px;
font-family: "Times New Roman", Times, serif;
}
#chequeSize:hover
{
color:black;
background-color:#ADEBAD;
}

#dCsize
{
color:white;
font-size:18px;
font-family: "Times New Roman", Times, serif;
}
#dCsize:hover
{
color:black;
background-color:#ADEBAD;
}

#pOsize
{
color:white;
font-size:18px;
font-family: "Times New Roman", Times, serif;
}
#pOsize:hover
{
color:black;
background-color:#ADEBAD;
}

#payrollSize
{
color:white;
font-size:18px;
font-family: "Times New Roman", Times, serif;
}
#payrollSize:hover
{
color:black;
background-color:#ADEBAD;
}

#profitLoss
{
color:white;
font-size:18px;
font-family: "Times New Roman", Times, serif;
}
#profitLoss:hover
{
color:black;
background-color:#ADEBAD;
}

#balanceSheet
{
color:white;
font-size:18px;
font-family: "Times New Roman", Times, serif;
}
#balanceSheet:hover
{
color:black;
background-color:#ADEBAD;
}
		
footer .navbar-collapse.in {
    bottom: 70px;
    position: absolute;
	color:white;
    width:100%;
}

.nav>li>a {
background-color: #248F24;
position: relative;
display: block;
padding: 10px 15px;
}

.navbar-inverse .navbar-collapse, .navbar-inverse .navbar-form {
border-color:white;

}

@media only screen and (max-width: 800px) {
	
	footer .navbar-collapse.in {
    bottom: 50px;
    position: absolute;
	color:white;
    width:100%;
}
	
}

.navbar-fixed-bottom {
bottom: 0;
margin-bottom: 86px;
border-width: 1px 29 0;
height: 0em;
}

@media only screen and (max-width: 800px) {
	
.navbar-fixed-bottom {
bottom: 0;
margin-bottom: 0px;
border-width: 1px 29 0;
height: 0em;
}	
	}
	

.footerbody
{
margin-top:1em;
}	

@media only screen and (max-width: 800px) {
	
.footerbody
{
margin-top:1em;
}
}	

.navbar-inverse {
background-color: #F7EDED;
border-color: #FFFAFA;
}
</style>
		
<!-------------footer nav menu buttons style  end--------------------->		
</head>
<body>


<div class="container">


<!--------------------logo track start--------------------->

<div class="container">

<div class="col-md-4 form-horizontal">
<img class="img-responsive" src="logo/logo.jpg.jpeg"  id="logosise" alt=""> 
</div>

<div class="col-md-8 form-horizontal">
<h1><b>Anybody Can Account</b></h1> 

<!--<img class="img-responsive" src="logo/logo1.jpg.png" alt="" width="200" height="200">--> 
</div>

</div>

<!--------------------logo track end--------------------->


<!---bootstrap navbar menu button-------------------->

<div class="container">
<div class="body-wrap">
<div class="container col-md-12 form-horizontal">
<nav id="myNavbar" class="navbar navbar-default" role="navigation">
<!-- Brand and toggle get grouped for better mobile display -->
<div class="container_fluid">
<div class="navbar-header">
<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<a class="navbar-brand" href="index.php" id="colorhr"><p class="serif">Company Detail</p></a>
</div>
<!-- Collect the nav links, forms, and other content for toggling -->
<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
<ul class="nav navbar-nav">
<li id=""><a  class="navbar-brand" href="" id="colorhome"><p class="serif">Item/Customer Detail</p></a></li>
<li id=""><a  class="navbar-brand" href="" id="colorpayroll"><p class="serif">Purchase Bill</p></a></li>
<li><a  class="navbar-brand" href="" id="colorpayslip"><p class="serif">Sales Bill</p></a></li>
<li><a  class="navbar-brand" href="" id="colorpayslip"><p class="serif">Payment</p></a></li>
<li><a  class="navbar-brand" href="" id="colorpayslip"><p class="serif">Recipt</p></a></li>
<li><a  class="navbar-brand" href="" id="colorpayslip"><p class="serif">Bank</p></a></li>
<li><a  class="navbar-brand" href="" id="colorpayslip"><p class="serif">Day Book</p></a></li>
<li><a  class="navbar-brand" href="" id="colorpayslip"><p class="serif">Quotation</p></a></li>
<li><a  class="navbar-brand" href="" id="colorpayslip"><p class="serif">Log Out</p></a></li>
<!--<li class="dropdown">
<a href="#" data-toggle="dropdown" class="dropdown-toggle">Messages <b class="caret"></b></a>
<ul class="dropdown-menu">
<li><a href="#">Inbox</a></li>
<li><a href="#">Drafts</a></li>
<li><a href="#">Sent Items</a></li>
<li class="divider"></li>
<li><a href="#">Trash</a></li>
</ul>
</li>-->
</ul>

            
</div><!-- /.navbar-collapse -->
        
</div>
</div>
</nav>
</div>
</div>


<!---bootstrap navbar menu button  end-------------------->
<div class=" container thumbnail">


<div class="container">
<h2 align="center">Day Book Details</h2>
</div>


<div class="container">
  
<div class="container row col-md-4">  
<table class="table table-bordered ">
  
<tr>
<th class="col-md-4" id="FromEndSearch">From Date</th>
<td><input type="text" id="fromDate" placeholder="Enter From Date"></td>
</tr>

<tr>
<th class="col-md-4"  id="FromEndSearch">End Date</th>
<td><input type="text"  class="ui-widget-content" id="endDate" placeholder="Enter End Date" ></td>
</tr>



</table>
<button type="button"  id="viewDetail" value="">View Detail</button>
</div>


<div class="container row col-md-4">  
<table class="table table-bordered ">
  
<tr>
<th class="col-md-2"> </th>
<td><input type="text" id="simpledash" ></td>
</tr>

<tr>
<th class="col-md-2"> </th>
<td><input type="text"  id="simpledash1" ></td>
</tr>



</table>
<button type="button" id="viewBillByAll" value="">View Bill By All</button>
</div>

<div class="container row col-md-4">  
<table class="table table-bordered ">
  
<tr>
<th class="col-md-4"  id="FromEndSearch"> &nbspSearch </th>
<td><select id="searchSize">
<option value="Select">Select..</option>
<option value="Company Name">Company Name</option>
<option value="Invoice number">Invoice number</option>
</select>
</td>
</tr>

<tr>
<th class="col-md-4" id="FromEndSearch"></th>
<td><input type="text" id="dashBox" ></td>
</tr>



</table>
<button class="" id="viewByName" type="button" value="">View-By Name</button>
</div>


</div>

</br>

<div class="container ">
    <div class="row col-md-12">

        <div id="no-more-tables" style="height:400px; background-color:orange;">
            <table class="col-md-12 table-bordered table-striped table-condensed cf">
				
        		<thead class="cf">
        			<tr>
        				<th id="coloramount">Date</th>
        				<th id="coloramount" >Perticulars</th>
        				<th  id="coloramount" class="numeric">Price</th>
        				<th id="coloramount"  class="numeric">Vch Type</th>
        				<th id="coloramount" class="numeric">Bill No</th>
        				<th id="coloramount" class="numeric">Debit</th>
        				<th id="coloramount" class="numeric">Credit</th>
        	
					
        			</tr>
				
        		</thead>
        		<tbody>
        			<tr >
				
        				<td data-title="Date">ABC</td>
        				<td data-title="Perticulars">AUSTRALIAN AGRICULTURAL COMPANY LIMITED.</td>
        				<td data-title="Price" class="numeric">$1.38</td>
        				<td data-title="Vch Type" class="numeric">-0.01</td>
        				<td data-title="Bill No" class="numeric">-0.36%</td>
        				<td data-title="Debit" class="numeric">$1.39</td>
        				<td data-title="Credit" class="numeric">$1.39</td>
        			
        			</tr>

 
        	
        		</tbody>
					
        	</table>
        </div>
    </div>

</div>

</br>

</div>

<!-------------footer nav menu buttons  start--------------------->	
	
<div class="container">
<footer>
    <div class="navbar navbar-inverse navbar-fixed-bottom" id="myNavbars">
        <div class="container" >
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#footer-body">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
<a class="navbar-brand" href="" id=""></a>
            </div>
            <div class="navbar-collapse collapse" id="footer-body">
                <ul class="nav navbar-nav">
				    <li><a href="#" id="Stock">Stock</a></li>
                    <li><a href="#" id="pettyCash">PettyCash</a></li>
                    <li><a href="#" id="generalLedger">General Ledger</a></li>
                    <li><a href="#" id="generalVoucher">General voucher</a></li>
                    <li><a href="#" id="peportSize">Report</a></li>
                    <li><a href="#" id="chequeSize">Cheque</a></li>
                    <li><a href="#" id="dCsize">DC</a></li>
                    <li><a href="#" id="pOsize">PO</a></li>
					<li><a href="#" id="payrollSize">Payroll</a></li>
					<li><a href="#" id="profitLoss">Profit and Loss</a></li>
					<li><a href="#" id="balanceSheet">Balance Sheet</a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>
</div>
<!-------------footer nav menu buttons  end--------------------->	
</br>
<div class="container" >
<footer id="footer" class="footerbody">
<h6 id="footercolor">Copyrights © 2014 " Billing Soft ". All Rights Reserved.</h6>
<h6 id="footercolor">Designed by :<a href="http://www.onesquaresolutions.com/" id="foo1" target="_blank">One Square Software Solution</a></h6>
</footer>
</div>



</div>
</body>
</html>