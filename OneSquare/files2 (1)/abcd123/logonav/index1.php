<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Logo Nav - Start Bootstrap Template</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/logo-nav.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

<!-------logo nav	style start---------------------------------->
	<style>

#imgsize
{
height:4em;
width:10em;
}

@media (min-width: 768px)
body
{
.navbar-nav>li>a {
line-height: 25px;
	padding: 15px 15px;

}
}

.navbar-nav>li>a {
	line-height: 15px;
	padding: 25px 25px;
	
}

.navbar-brand {
float: left;
height: 5em;
padding: 11px 11px;
font-size: 20px;
line-height: 51px;
}

.navbar-default
{
margin-top:-100px;	
}

.navbar-toggle {
position: relative;
float: right;
padding: 9px 10px;
margin-top: 33px;
margin-right: 15px;
margin-bottom: 8px;
background-color: transparent;
background-image: none;
border: 1px solid transparent;
border-radius: 4px;
}
	</style>

<!-------logo nav	style start end---------------------------------->	
	
<!----carousel style start------------------------>
  <style>
  .carousel-inner > .item > img,
  .carousel-inner > .item > a > img {
    width: 50%;
      margin: auto;
	  height:20em;
	  float:left;
	  margin-top:0em;
  }
  
  .carousel-inner {
position: relative;
width: 100%;
overflow: hidden;
margin-top: -40px;
}
  </style>	
<!----carousel style end------------------------>	

	
	<!-----social side bar style start------------------------->
	<style type="text/css">
	body{
		/*height: 10000px;*/
		font-family: "Lato";

	}
	.sticky-container{
		/*background-color: #333;*/
		padding: 0px;
		margin: 0px;
		position: fixed;
		right: -119px;
		top:130px;
		width: 200px;
	}
	.sticky li{
		list-style-type: none;
		background-color: #333;
		color: white;
		height: 43px;
		padding: 0px;
		margin: 0px 0px 1px 0px;
		-webkit-transition:all 0.25s ease-in-out;
		-moz-transition:all 0.25s ease-in-out;
		-o-transition:all 0.25s ease-in-out;
		transition:all 0.25s ease-in-out;
		cursor: pointer;
		filter: url("data:image/svg+xml;utf8,<svg xmlns=\'http://www.w3.org/2000/svg\'><filter id=\'grayscale\'><feColorMatrix type=\'matrix\' values=\'0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0 0 0 1 0\'/></filter></svg>#grayscale"); 
                filter: gray; 
                -webkit-filter: grayscale(100%); 
	}
	.sticky li:hover{
		margin-left: -115px;
		/*-webkit-transform: translateX(-115px);
		-moz-transform: translateX(-115px);
		-o-transform: translateX(-115px);
		-ms-transform: translateX(-115px);
		transform:translateX(-115px);*/
		/*background-color: #8e44ad;*/
		filter: url("data:image/svg+xml;utf8,<svg xmlns=\'http://www.w3.org/2000/svg\'><filter id=\'grayscale\'><feColorMatrix type=\'matrix\' values=\'1 0 0 0 0, 0 1 0 0 0, 0 0 1 0 0, 0 0 0 1 0\'/></filter></svg>#grayscale");
                -webkit-filter: grayscale(0%);
	}
	.sticky li img{
		float: left;
		margin: 5px 5px;
		margin-right: 10px;
	}
	.sticky li p{
		padding: 0px;
		margin: 0px;
		text-transform: uppercase;
		line-height: 43px;
		background-color:orange;
	}
	
	</style>
	
<!-----social side bar style start end------------------------->



	
<!---footer style start------------------------------->	
<style>
	
#footer{

background-color: #f3f3f3;
border-top: 4px solid #f78e21;
margin-top: 0px;
padding-top: 32px;

}
 
h4, .h4 {
font-size: 18px;
} 

.h4, .h5, .h6, h4, h5, h6 {
margin-top: -5px;
margin-bottom: 10px;
}

#pstyle
 {
font-family: 'Open Sans', sans-serif;
font-size: 13px;
color: #666;


display: block;
-webkit-margin-before: 1em;
-webkit-margin-after: 1em;
-webkit-margin-start: 0px;
-webkit-margin-end: 0px;
}
	</style>
<!---footer style end------------------------------->

		
</head>

<body>

<!-------logo nav start start---------------------------------->	
<div class="container">
<div class="body-wrap">
    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-default" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">
                    <img  id="imgsize" src="logo/Koala.jpg" alt="">
                </a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div  class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav  navbar-right">
                    <li>
                        <a href="#">About</a>
                    </li>
                    <li>
                        <a href="#">Services</a>
                    </li>
                    <li>
                        <a href="#">Contact</a>
                    </li>
					  <li>
                        <a href="#">Contact</a>
                    </li>
					  <li>
                        <a href="#">Contact</a>
                    </li>
				
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
	</div>
		</div>
	
<!-------logo nav start end---------------------------------->	


<!----carousel style start------------------------>
<div class="container">
  <br>
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
      <li data-target="#myCarousel" data-slide-to="3"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
      <div class="item active">
        <img src="img_chania.jpg" alt="" width="460" height="345">
		   <img src="img_chania.jpg" alt="" width="460" height="345">
      </div>

      <div class="item">
        <img src="img_chania2.jpg" alt="" width="460" height="345">
		   <img src="img_chania.jpg" alt="" width="460" height="345">
      </div>
    
      <div class="item">
        <img src="img_flower.jpg" alt="" width="460" height="345">
		   <img src="img_chania.jpg" alt="" width="460" height="345">
      </div>

      <div class="item">
        <img src="img_flower2.jpg" alt="" width="460" height="345">
		   <img src="img_chania.jpg" alt="" width="460" height="345">
      </div>
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>
	
<!----carousel style end------------------------>


	<!-----social side bar  start start------------------------->	
	<div class="sticky-container">
		<ul class="sticky">
			<li>
				<img width="32" height="32" title="" alt="" src="img/fb.jpg" />
				<p>Facebook</p>
			</li>
			<li>
				<img width="32" height="32" title="" alt="" src="img/t.jpg" />
				<p>Twitter</p>
			</li>
			<li>
				<img width="32" height="32" title="" alt="" src="img/lin.jpg" />
			<p>Linkedin</p>
			</li>



		</ul>
	</div>
	<!-----social side bar  start start------------------------->
	
<!----------------------solution body start---------------------->	
<div class="container">

<h1 id="paragraph" >Welcome To One Square Solution </h1>
 <div class="panel panel-default">
<div class="panel-body" id="para" >
<h1 id="ser">"Solutions"</h1>
One Square Solutions is a single platform for all your system hardware and IT requirements. We have the expertise and experience in providing customized solutions for organizations in different sectors. Our hardware solutions and maintenance services are considered to be among the best in the whole country. We offer complete end-to-end hardware solutions from architecture and procurement to installation, testing and maintenance.</br>
We offer onsite and offsite service, our onsite services include deployment of the best available talented service engineers. Our range of solutions includes server installation and maintenance, our expertise in this field lies in Windows servers, Linux servers and Solaris.</br>
Other solutions offered by us include Router Firewall installation and maintenance of Cisco router and firewall, D-Link router and firewall, and Net Gear router and firewall; data backup which includes tape backup, data backup on disk and remote data backup; different kinds of printer maintenance, network deployment, PC hardware repair and upgrade, software installation, small application development, all kinds of mother boards and logic boards, disaster recovery, wireless network installation, and Virus, Spyware and Trojan removal.</br>


</div>
</div>
</div>
	
<!----------------------solution body start---------------------->		
	
<!---footer body start------------------------------->		
<div class="container">
<div class="container col-md-12 form-horizontal">
<div class="row">
<footer id="footer">


<div class="col-md-4">
<h4 class="footer-title">
 &nbsp About JSpiders</h4>
<p id="pstyle">&nbsp JSpiders is No.1 JAVA/J2EE training institute in India with a view to 
  &nbsp  bridge the gap between Industry Requirement and curriculum of 
  &nbsp&nbspeducational institutions and also to meet the ever increasing &nbsp&nbspdemand for Quality IT professional.
</p>

</div>


<div class="row">
<div class="col-md-4">
<a name="call"></a>
<h4 class="footer-title">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Contact Information
</h4>
<ul>
✆ Basavanagudi:
<br>	
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 9686114422 / 9686995511
<br>
									                                        
✆ Old Airport Road: 
<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 9686114422 / 9686995511
<br>

<p><span class="glyphicon glyphicon-envelope"> shashidharven@gmail.com</span></p>    

</ul>
										
</div>



<div class="col-md-3">
<h4 class="">
More info
</h4>
<p id="pstyle">We have multifarious
 activities encompassing java training, Placements, Deployment of profecessionals and technology research.</p>
 

</div>

</div>
</footer>
</div>

</div>	
</div>

<div class="container_fluid">

<div class="bottom-footer">
        <div style="text-align:center;padding: 10px 0 10px 0; background: ;">
          <p>
            Copyright © 2014 
            <a href="#">
              Jspiders
            </a>
          </p>
        </div>
      </div>



</div>	

<!---footer body end------------------------------->	
    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
