<!DOCTYPE html>
<html lang="en" class="demo2 no-js">
	<head>
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
		<title>Create an Immersive Slider Experience with Immersive Slider</title>
		<meta name="description" content="Let me present to you Immersive Slider, a plugin I built to help developers create a more immersive slider experience by changing the whole container" />
		<meta name="keywords" content="jquery slider, immersive slider, featured slider, sliding panel" />
		<meta name="author" content="Author for Onextrapixel" />
		<link rel="shortcut icon" id="favicon" href="favicon.png"> 
		<link rel="stylesheet" type="text/css" href="css/default.css" />
		
		<!-- Edit Below -->
		<script type="text/javascript" src="http://code.jquery.com/jquery-1.9.1.js"></script>
	    <script type="text/javascript" src="js/jquery.immersive-slider.js"></script>
	    <link rel="stylesheet" type="text/css" href="css/style.css" />
	    <link rel="stylesheet" type="text/css" href="css/immersive-slider.css" />
	    <link rel="stylesheet" type="text/css" href="css/elusive-webfont.css" />
	    <script src="../file/js/modernizr.js"></script>
	</head>
	<body class="demo2">

	<div class="nav-top clearfix">
		<span class="right"><a class="nav-icon icon-th" href="http://www.onextrapixel.com/category/tutorials/"><span>View all Tutorials</span></a></span>
		<span class="right"><a class="nav-icon icon-left" href="http://www.onextrapixel.com/2013/10/30/creating-an-immersive-slider-experience-with-jquery-immersive-slider/"><span>Back to Tutorial Article</span></a></span>
	</div>
  
	<div class="header">
		<h1>jQuery Immersive Slider: Fade Animation</h1>
		<div class="menu">
	  		<a href="index1.php">Demo 1</a>
	  		<a class="active" href="index2.php">Demo 2</a>
	  		<a href="index3.php">Demo 3</a>
	  		<a href="index4.php">Demo 4</a>
	  		<a href="index5.php">Demo 5</a>
	  	</div>
	</div>
	
	<div class="main">
    <div class="page_container">
      <div id="immersive_slider">
        <div class="slide" data-blurred="img/slide1_blurred.jpg">
          <div class="content">
            <h2>Immersive Slider</h2>
            <p>It’s never been easier to watch Immersive Slider on the big screen
            Send your favorite Immersive Slider videos from your Android phone or tablet to TV with the touch of a button. It’s easy. No wires, no setup, no nothing. Find out more here.</p>
          </div>
          <div class="image">
            <img src="img/slide1.jpg" alt="Slider 1">
          </div>
        </div>
        <div class="slide" data-blurred="img/slide2_blurred.jpg">
          <div class="content">
            <h2>Immersive Slider</h2>
            <p>It’s never been easier to watch Immersive Slider on the big screen
            Send your favorite Immersive Slider videos from your Android phone or tablet to TV with the touch of a button. It’s easy. No wires, no setup, no nothing. Find out more here.</p>
          </div>
          <div class="image">
            <img src="img/slide2.jpg" alt="Slider 1">
          </div>
        </div>
        <div class="slide" data-blurred="img/slide3_blurred.jpg">
          <div class="content">
            <h2>Immersive Slider</h2>
            <p>It’s never been easier to watch Immersive Slider on the big screen
            Send your favorite Immersive Slider videos from your Android phone or tablet to TV with the touch of a button. It’s easy. No wires, no setup, no nothing. Find out more here.</p>
          </div>
          <div class="image">
            <img src="img/slide3.jpg" alt="Slider 1">
          </div>
        </div>
        <div class="is-nav">
          <a href="#" class="is-prev">&laquo;</a>
          <a href="#" class="is-next">&raquo;</a>
        </div>
      </div>
    </div>
	</div>
	<div class="benefits">
    <div class="page_container">
	   
	  </div>
	</div>
	<script type="text/javascript">
	  $(document).ready( function() {
	    $("#immersive_slider").immersive_slider({
	      container: ".main",
	      animation: "fade",
	      autoStart: false
	    });
	  });
    
  	</script>
		
	</body>
</html>