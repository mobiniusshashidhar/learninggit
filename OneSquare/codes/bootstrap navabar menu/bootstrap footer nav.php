<!DOCTYPE html>
<html>
	<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<!-------------footer nav menu buttons style  start--------------------->	
<style>
#myNavbar
{
background-color:#248F24;
}	

#Stock
{
color:white;
font-size:18px;
font-family: "Times New Roman", Times, serif;
}
#Stock:hover
{
color:black;
background-color:#ADEBAD;
}


#pettyCash
{
color:white;
font-size:18px;
font-family: "Times New Roman", Times, serif;
}
#pettyCash:hover
{
color:black;
background-color:#ADEBAD;
}

#generalLedger
{
color:white;
font-size:18px;
font-family: "Times New Roman", Times, serif;
}
#generalLedger:hover
{
color:black;
background-color:#ADEBAD;
}

#generalVoucher
{
color:white;
font-size:18px;
font-family: "Times New Roman", Times, serif;
}
#generalVoucher:hover
{
color:black;
background-color:#ADEBAD;
}

#peportSize
{
color:white;
font-size:18px;
font-family: "Times New Roman", Times, serif;
}
#peportSize:hover
{
color:black;
background-color:#ADEBAD;
}

#chequeSize
{
color:white;
font-size:18px;
font-family: "Times New Roman", Times, serif;
}
#chequeSize:hover
{
color:black;
background-color:#ADEBAD;
}

#dCsize
{
color:white;
font-size:18px;
font-family: "Times New Roman", Times, serif;
}
#dCsize:hover
{
color:black;
background-color:#ADEBAD;
}

#pOsize
{
color:white;
font-size:18px;
font-family: "Times New Roman", Times, serif;
}
#pOsize:hover
{
color:black;
background-color:#ADEBAD;
}

#payrollSize
{
color:white;
font-size:18px;
font-family: "Times New Roman", Times, serif;
}
#payrollSize:hover
{
color:black;
background-color:#ADEBAD;
}

#profitLoss
{
color:white;
font-size:18px;
font-family: "Times New Roman", Times, serif;
}
#profitLoss:hover
{
color:black;
background-color:#ADEBAD;
}

#balanceSheet
{
color:white;
font-size:18px;
font-family: "Times New Roman", Times, serif;
}
#balanceSheet:hover
{
color:black;
background-color:#ADEBAD;
}
		
footer .navbar-collapse.in {
    bottom: 70px;
    position: absolute;
	color:white;
    width:100%;
}

.nav>li>a {
background-color: #248F24;
position: relative;
display: block;
padding: 10px 15px;
}

.navbar-inverse .navbar-collapse, .navbar-inverse .navbar-form {
border-color:white;

}

@media only screen and (max-width: 800px) {
	
	footer .navbar-collapse.in {
    bottom: 50px;
    position: absolute;
	color:white;
    width:100%;
}
	
}


</style>
		
<!-------------footer nav menu buttons style  end--------------------->			
	</head>

	<body>
<!-------------footer nav menu buttons  start--------------------->	
	
<div class="container">
<footer>
    <div class="navbar navbar-inverse navbar-fixed-bottom" role="navigation" id="myNavbar">
        <div class="container" >
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#footer-body">
				<span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
<a class="navbar-brand" href="" id=""></a>
            </div>
            <div class="navbar-collapse collapse " id="footer-body">
                <ul class="nav navbar-nav">
				 <li><a href="#" id="Stock">Stock</a></li>
                    <li><a href="#" id="pettyCash">PettyCash</a></li>
                    <li><a href="#" id="generalLedger">General Ledger</a></li>
                    <li><a href="#" id="generalVoucher">General voucher</a></li>
                    <li><a href="#" id="peportSize">Report</a></li>
                    <li><a href="#" id="chequeSize">Cheque</a></li>
                    <li><a href="#" id="dCsize">DC</a></li>
                    <li><a href="#" id="pOsize">PO</a></li>
					<li><a href="#" id="payrollSize">Payroll</a></li>
					<li><a href="#" id="profitLoss">Profit and Loss</a></li>
					<li><a href="#" id="balanceSheet">Balance Sheet</a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>
</div>
<!-------------footer nav menu buttons  end--------------------->	
	</body>
</html>