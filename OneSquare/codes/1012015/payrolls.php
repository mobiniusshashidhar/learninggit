<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
 
<!--DIALOG SCRIPTS---> 
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>


  
<!--<script>
	//add customer dialog box		
					
  $(function() {
    $( "#dilaugepopup" ).dialog({
      autoOpen: false,
	
       width: 500,
	  height: 250,
	  
      show: {
        effect: "blind",
        duration: 1000
		
      },
	  
      hide: {
        effect: "explode",
        duration: 1000
      }
    });
 
    $( "#addcustomerpopup" ).click(function() {
      $( "#dilaugepopup" ).dialog( "open" );
	  
	  
    });
  });
</script>-->
<script>
   $(function() {
   $('#fromdate').datepicker({ dateFormat: 'yy-mm-dd' }).val();
 $('#enddate').datepicker({ dateFormat: 'yy-mm-dd' }).val();
  });
</script>
<script>
//earnings dialog box window


  $(function() {
    $( "#additemdilauge" ).dialog({
      autoOpen: false,
      show: {
        effect: "blind",
        duration: 1000
      },
      hide: {
        effect: "explode",
        duration: 1000
      }
    });
 
    $( "#additempopup" ).click(function() {
      $( "#additemdilauge" ).dialog( "open" );
    });
  });

  
 //earning  mouse enter and mouse leave script
 
$(document).ready(function(){
    $("#additempopup").hide();

$("#earning").mouseenter(function(){
    $("#additempopup").show();

$("#earning").mouseleave(function(){
    $("#additempopup").hide();	
	
});
});
});
</script>

<script>
//deduction  dialog box window

  $(function() {
    $( "#deductionpopup" ).dialog({
      autoOpen: false,
      show: {
        effect: "blind",
        duration: 1000
      },
      hide: {
        effect: "explode",
        duration: 1000
      }
    });
 
    $( "#deductionaddpopup" ).click(function() {
      $( "#deductionpopup" ).dialog( "open" );
    });
  });
 
 
   
 //deduction  mouse enter and mouse leave script
 
$(document).ready(function(){
    $("#deductionaddpopup").hide();

$("#deduction").mouseenter(function(){
    $("#deductionaddpopup").show();

$("#deduction").mouseleave(function(){
    $("#deductionaddpopup").hide();	
	
});
});
});
</script> 



<style>
#image
{
float:right;
}
	
#itemdetails
{
text-align:center;
}	

div
{
padding:1px;
}

#companysize
{
border: 0px solid white;
width:12em;
}

#companysize:hover
{
background-color:#D6FFAD;
}


#textareasize
{
border: 0px solid white;

width:12em;
}
#textareasize:hover
{
background-color:#D6FFAD;
}


#customernamesize
{
border: 0px solid white;
width:12em;
}

#customernamesize:hover
{
background-color:#D6FFAD;
}


#sizetextarea
{
border: 0px solid white;
width:12em;
}

#sizetextarea:hover
{
background-color:#D6FFAD;
}


#addcustomerpopup
{
border: 0px solid white;
}


#basicpay
{	
border: 0px solid white;
width:6em;
}
#basicpay:hover
{
background-color:#D6FFAD;	
}


#dearnessallowance
{
border: 0px solid white;
width:6em;
}
#dearnessallowance:hover
{
background-color:#D6FFAD;	
}

#conveyanceallowance
{
border: 0px solid white;
width:6em;
}
#conveyanceallowance:hover
{
background-color:#D6FFAD;
}

#houserentallowance
{
border: 0px solid white;
width:6em;
}
#houserentallowance:hover
{
background-color:#D6FFAD;
}	


#medicalallowance
{
border: 0px solid white;
width:6em;

}
#medicalallowance:hover
{
background-color:#D6FFAD;
}



#totalearning
{
border: 0px solid white;
}
#totalearning:hover
{
background-color:#D6FFAD;
}


#totaldeduction
{
border: 0px solid white;
}
#totaldeduction:hover
{
background-color:#D6FFAD;
}

#Previousbalance
{
border: 0px solid white;
}
#Previousbalance:hover
{
background-color:#D6FFAD;
}

#NetPayRounded
{
border: 0px solid white;
}
#NetPayRounded:hover
{
background-color:#D6FFAD;
}


#CarryOverRoundOff
{
border: 0px solid white;
}
#CarryOverRoundOff:hover
{
background-color:#D6FFAD;
}


#bordertabsize
{
width:100%;
border:solid 2px black;
}	

#basicpaycolor
{

background-color:#FFD119;
border: 1px solid black;
}

#dearnesscolor
{
background-color:#FFD119;
border: 1px solid black;
}

#conveyancecolor
{
background-color:#FFD119;
border: 1px solid black;
}

#houserentcolor
{
background-color:#FFD119;
border: 1px solid black;
}

#medicalcolor
{
background-color:#FFD119;
border: 1px solid black;
}

#rowthumbnail
{
border:2px solid black;
}	

#deduction
{
background-color:#FFD119;
border: 1px solid black;
}

#coloramount
{
background-color:#FFD119;
border: 1px solid black;
}

#colorcompany
{
background-color:#FFD119;
border: 1px solid black;
}

#coloremp
{
background-color:#FFD119;
border: 1px solid black;
}

#earning
{
background-color:#FFD119;
border: 1px solid black;
}

#coloramountsize
{
background-color:#FFD119;
border: 1px solid black;
}

#colorsizecompany
{
background-color:#FFD119;
border: 1px solid black;
}

#coloremployeesize
{
background-color:#FFD119;
border: 1px solid black;
}

#deductionaddpopup:hover
{
background-color:#D6FFAD;
}

#additempopup:hover
{
background-color:#D6FFAD;	
}

.minusbtn:hover
{
background-color:#D6FFAD;	
}	

.dedminusbtn:hover
{
background-color:#D6FFAD;	
}
#logosise
{
width:150px;
height:65px;
float:left;
}


</style>

<!---bootstrap navbar menu button style-------------------->
 <style>
#myNavbar
{
background-color:#248F24;

}
 
#colorhr
{
color:white;
font-size:18px;
}
#colorhr:hover
{
color:black;
background-color:#ADEBAD;
}


#colorhome
{
color:white;
font-size:18px;
}
#colorhome:hover
{
color:black;
background-color:#ADEBAD;
}



#colorpayroll
{
color:white;
font-size:18px;
}
#colorpayroll:hover
{
background-color:#ADEBAD;
color:black;
}



#colorpayslip
{
color:white;
font-size:18px;
}
#colorpayslip:hover
{
background-color:#ADEBAD;
color:black;
}


#colorlogout
{
color:white;
font-size:18px;
}

#colorlogout:hover
{
background-color:#ADEBAD;
color:black;
}


p.serif 
{
font-family: "Times New Roman", Times, serif;
}




</style>   

<!---bootstrap navbar menu button style end-------------------->
<style>
#footer
{
float:right;
}	
#footercolor
{
color:#3399CC;
}
#foo1
{
color:black;	
}
#textareatext
{
width:100%;	
text-align: center;
font-family: "Myriad Pro", "Gill Sans", "Gill Sans MT", Calibri, sans-serif;
}
#textareatext:hover
{
background-color:#D6FFAD;	
}
#payrollsubmit
{
background-color:#248F24;
color:white;
border-radius: 10px;	
width:10em;
}
#payrollsubmit:hover
{
color:black;
background-color:#ADEBAD;	
}
</style>


<style>
.textboxsize {
 border: none;
 width: 100%;
}
#sizeinput {
font-size: 17px;
height: 30px;	
}
#sizetable {
	 background: none repeat scroll 0 0 #;
    border: 2px solid #000;	
}
</style>
<script>
//"earning" nth number of text box

$(document).ready(function(){
     $('.plusbtn').click(function() {
		 var i = document.getElementById("index").value ;
		  alert(i) ;
           $("#sizetable").append('<tr><td><input type="text" id="sizeinput" class="textboxsize" value="" /></td><td><input type="text"  id="sizeinput" class="textboxsize" value="" /></td><td><input type="text" id="sizeinput" class="textboxsize" value="" /></td><td><input type="text" id="sizeinput" class="textboxsize" value="" /></td></tr>');
			i++ ;
			document.getElementById("index").value = i ;
	 });
   $('.minusbtn').click(function() {
       if($("#sizetable tr").length != 2)
         {
            $("#sizetable tr:last-child").remove();
         }
      else
         {
            alert("You cannot delete first row");
         }
    });
});
</script>



<style>
.dedtextbox1 {
 border: none;
 width: 100%;
}
#dedsizeinput {
	font-size: 17px;
    height: 30px;	
}
#dedsizetable1 {
	 background: none repeat scroll 0 0 #;
    border: 2px solid black;	
}

</style>
<script>

//"deduction" nth number of text box

$(document).ready(function(){
     $('.dedplusbtn').click(function() {
           $("#dedsizetable1").append('<tr><td><input type="text" id="dedsizeinput" class="dedtextbox1" value="" /></td><td><input type="text"  id="dedsizeinput" class="dedtextbox1" value="" /></td><td><input type="text" id="dedsizeinput" class="dedtextbox1" value="" /></td><td><input type="text" id="dedsizeinput" class="dedtextbox1" value="" /></td></tr>');
     });
   $('.dedminusbtn').click(function() {
       if($("#dedsizetable1 tr").length != 2)
         {
            $("#dedsizetable1 tr:last-child").remove();
         }
      else
         {
            alert("You cannot delete first row");
         }
    });
});
</script>

  <!--SIMPLE SIDE BAR STYLE START------>
 <style>
 
 body,html{
    height: 100%;
  }

  nav.sidebar, .main{
    -webkit-transition: margin 200ms ease-out;
      -moz-transition: margin 200ms ease-out;
      -o-transition: margin 200ms ease-out;
      transition: margin 200ms ease-out;
  }

  .main{
    padding: 10px 10px 0 10px;
  }
 
  @media (min-width: 765px) {
	 
	  
	  .main{
      position: absolute;
      width: calc(100% - 40px); 
      margin-top: 40px;
      float: right;
    }  

    nav.sidebar:hover + .main{
      margin-top: 200px;
    }

    nav.sidebar.navbar.sidebar>.container .navbar-brand, .navbar>.container-fluid .navbar-brand {
      margin-top: 0px;
    }

    nav.sidebar .navbar-brand, nav.sidebar .navbar-header{
      text-align: center;
      width: 100%;
      margin-top: 0px;
    }
    
    nav.sidebar a{
      padding-right: 13px;
	  	
    }

    nav.sidebar .navbar-nav > li:first-child{
      border-top: 1px #e5e5e5 solid;
    }

    nav.sidebar .navbar-nav > li{
      border-bottom: 1px #e5e5e5 solid;
    }

    nav.sidebar .navbar-nav .open .dropdown-menu {
      position: static;
      float: none;
      width: auto;
      margin-top: 0;
      background-color: transparent;
      border: 0;
      -webkit-box-shadow: none;
      box-shadow: none;
    }

    nav.sidebar .navbar-collapse, nav.sidebar .container-fluid{
      padding: 0 0px 0 0px;
    }

    .navbar-inverse .navbar-nav .open .dropdown-menu>li>a {
     
    }	  
	  
	  
nav.sidebar
 {
      width: 100%;
      height: 28%;
      margin-top: -180px;
      float: left;
      margin-bottom:0px;
 

	  

 }
  
  
    nav.sidebar li {
      width: 100%;
	 	
    }

    nav.sidebar:hover{
      margin-top: 0px;
	  float:left;
	  width:100%;
	  
	
	
    }

    .forAnimate{
      opacity: 0;
    }

  }  
  
    @media (min-width: 1330px) {

    .main{
      width: calc(100% - 200px);
      margin-top: 200px;
    }

    nav.sidebar{
      margin-top: 0px;
      float: left;
    }

    nav.sidebar .forAnimate{
      opacity: 1;
    }
	

  }

  nav.sidebar .navbar-nav .open .dropdown-menu>li>a:hover, nav.sidebar .navbar-nav .open .dropdown-menu>li>a:focus {
    color: #CCC;
    background-color: transparent;
  }

  nav:hover .forAnimate{
    opacity: 1;
  }
  section{
    padding-left: 15px;
  }
  

</style>
<!--SIMPLE SIDE BAR STYLE START END------>

<style>

#floatcenter
{
width:100%;
}

#imageheight
{
width:100%;
}
#fromdate
{
width:7em;	
}

#fromdate:hover
{
background-color:#D6FFAD;	
}

#enddate
{
width:7em;	
}
#enddate:hover
{
background-color:#D6FFAD;	
}
#defaulttext:hover
{
background-color:#D6FFAD;	
}

#buutonsize
{
width:7em;
height:2em;	
}

#namelable
{
width:7em;	
}
#namelable:hover
{
background-color:#D6FFAD;	
}
#billsize
{
width:10em;
height:2em;	
}

#sizesdivelement
{
background-color:red;
}
#selectsize
{
width:7em;
}
#leftcontainer
{
float:left;	
}
</style>

    <!----------------------vertical button style start--------------->
  <style>
  

		
  .hp-ctn-howItWorks
{
    position:fixed;
    top:50px;
    right: 0px;
    padding:0px;
    margin:0px;
    width: 30px;
    height:47px;
    background:#FF931E;
    z-index:15;
    border-radius: 3px 0px 0px 3px;
		margin-top:-50px;
}

.hp-ctn-howItWorks p
{
    margin: 15px 0px 0px 13px;
}

.hp-ctn-howItWorks p
{
    color: #fff;
    -moz-transform:rotate(-90deg);
    -ms-transform:rotate(-90deg);
    -o-transform:rotate(-90deg);
    -webkit-transform:rotate(-90deg);
}

#printcolor
{
color:white;
}
#printcolor:hover
{
color:black;
}	

	
  </style>
  
  <!----------------------vertical button style end--------------->

<!-------window print script start------------>
<script>
function printthis()
{
           
            var divContents = $("#divelementprint").html();
            var w = window.open('', '', 'height=600,width=1000');
            w.document.write('<html><head><title>DIV Contents</title>');
            w.document.write('</head><body >');
            w.document.write(divContents);
             w.document.write("<link rel='stylesheet' type='text/css' href='css/WndowPrint.css' />");
            
            w.document.write('</body></html>');
            w.document.close();
           javascript:w.print();
 w.close();
 window.close();
 return false;
}
</script>

<!-------window print script start------------>


</head>

<body>

<!--SIMPLE SIDE BAR  HTML TAGS START------>


<nav  class="navbar navbar-default sidebar" role="navigation" >
<div class="container">

	
<div class="navbar-header">
<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-sidebar-navbar-collapse-1">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>  


</div>


<div class="collapse navbar-collapse " id="bs-sidebar-navbar-collapse-1">
<ul class="nav navbar-nav ">

<div class="container">


<div class="col-md-3" >
<div class="form-horizontal form-group form-group-lg " id="">

<li class="active col-md"><label  class="col-sm-7 control-label" for="">From Date:</label></li> 
<div class="col-sm-5">       
<li class="active col-md-"><input type="text" class="" id="fromdate" placeholder=""></li>
</div>

<li class="active col-md"><label  class="col-sm-7 control-label" for="">End Date:</label></li> 
<div class="col-sm-5">       
<li class="active col-md-"><input type="text" class="" id="enddate" placeholder=""></li></br>
<li class="active col-md"><button type="button"  class=" " id="buutonsize" for="" >View Detail</button></li> 
</div>


</div>
</div>


<div class="col-md-3">
<div class=" form-horizontal form-group form-group-lg" id="">


<li class="active"><label class="col-sm-7 control-label"  for="lg">Name:</label></li>   
<div class="col-sm-5">  
<li class="active" id=""><select id="selectsize">
  <option value="volvo">Customer Name</option>
  <option value="saab">Invoice Number</option>
  <option value="opel">Opel</option>
  <option value="audi">Audi</option>
</select>
</li>

<li class="active col-md"><label class=" " for="lg"></label></li> 
<li class="active col-md-"><input type="text" class="" id="namelable" placeholder=""></li>
<li class="active"><td><button type="button" class="" id="buutonsize" placeholder="" value=""> View Details</button></td></li></br>
</div>

<button type="button" class="" id="billsize" placeholder="" value=""> View Bill By Date And Other Details</button>

</div>
</div>


<div class="col-md-6">
<div  class="" id="divelementprint" style="border:1px solid black;width:100%;height:180px;background-color:red;">

<center>
<h1>output</h1>
</center>
</div>

</div>


<div class="hp-ctn-howItWorks">
<p><a onclick="printthis();" id="printcolor">Print</a></p>
</div>



</div>

</ul>
</div>



</div>
</nav>
<img class="img-responsive imageside"  id="imageheight" src="logo/sea.png" alt="" >


<!--SIMPLE SIDE BAR  HTML TAGS END------>






<!--<input type="text" id="index" value="0"/>-->
<div class=" container col-md-12 form-horizontal">

<div class="container">

<div class="col-md-4 form-horizontal">
<img class="img-responsive" src="logo/logo.jpg.jpeg"  id="logosise" alt=""> 
</div>

<div class="col-md-8 form-horizontal">
<h1><b>Anybody Can Account</b></h1> 

<!--<img class="img-responsive" src="logo/logo1.jpg.png" alt="" width="200" height="200">--> 
</div>

</div>





<!---bootstrap navbar menu button-------------------->

<div class="body-wrap">
<div class="container col-md-12 form-horizontal">
<nav id="myNavbar" class="navbar navbar-default" role="navigation">
<!-- Brand and toggle get grouped for better mobile display -->
<div class="container_fluid">
<div class="navbar-header">
<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<a class="navbar-brand" href="#" id="colorhr"><p class="serif">HR</p></a>
</div>
<!-- Collect the nav links, forms, and other content for toggling -->
<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
<ul class="nav navbar-nav">
<li id=""><a  class="navbar-brand" href="#" id="colorhome"><p class="serif">Home</p></a></li>
<li id=""><a  class="navbar-brand" href="#" id="colorpayroll"><p class="serif">Payroll</p></a></li>
<li><a  class="navbar-brand" href="contacts.html" id="colorpayslip"><p class="serif">Payslip</p></a></li>
<!--<li class="dropdown">
<a href="#" data-toggle="dropdown" class="dropdown-toggle">Messages <b class="caret"></b></a>
<ul class="dropdown-menu">
<li><a href="#">Inbox</a></li>
<li><a href="#">Drafts</a></li>
<li><a href="#">Sent Items</a></li>
<li class="divider"></li>
<li><a href="#">Trash</a></li>
</ul>
</li>-->
</ul>
<ul class="nav navbar-nav navbar-right">
<li><a  class="navbar-brand" href="contacts.html" id="colorlogout"><p class="serif">Log Out</p></a></li>
<!--<li class="dropdown">
<a href="#" data-toggle="dropdown" class="dropdown-toggle">logout <b class="caret"></b></a>
<ul class="dropdown-menu">
<li><a href="#">Action</a></li>
<li><a href="#">Another action</a></li>
<li class="divider"></li>
<li><a href="#">Settings</a></li>
</ul>
</li>-->
</ul>
            
</div><!-- /.navbar-collapse -->
        
</div>
</div>
</nav>
</div>


<!---bootstrap navbar menu button  end-------------------->

<div class="container">
<div class="row thumbnail" id="rowthumbnail">

<div class="container">

<div class="col-md-6 form-horizontal">

<div class="form-group">

<label class="" for="">From:</label><br/>
<input type="text" class=" " id="companysize" placeholder=" Your Company Name">
<div class="col-sm-10 input-group"> 
<textarea class="" rows="4" id="textareasize" placeholder=" Your Company Address"></textarea>
</div>

</div>

</div>

<div class="col-md-6">
<div class="col-sm-14">
<img class="img-responsive"  id="image" src="logo/logo3.jpg.png" alt="" width="200" height="200"  > 
</div>
</div>

</div>




<div class="container">

<div class="col-md-5 form-horizontal ">

<div class="form-group">

<label class="" for="">To:</label><br/>
<input type="text" class="" id="customernamesize" placeholder=" Selected Customer Name">
<div class="col-sm-10  input-group">
<textarea class="" rows="4" id="sizetextarea" placeholder=""></textarea>
</div>

</div>

</div>



<div class="col-md-3" >
<button type="button" id="addcustomerpopup" class="btn btn-default"> <a href="index.php" target="_blank"> Add Employee </a></button>
</div>



<div class="col-md-4" >

<table class="  table-bordered " id="bordertabsize">


<tbody class="form-group">


<tr>
<td id="basicpaycolor"><label class=""  for="">Basic Pay</label></td>        
<td><input type="text" class="" id="basicpay" placeholder=""></td>
</tr>


<tr>
<td id="dearnesscolor"><label class="" for="">Dearness </label></td>         
<td><input type="text" class="" id="dearnessallowance" placeholder=""></td>
</tr>

<tr>
<td id="conveyancecolor"><label class="" for="">Conveyance</label></td>       
<td><input type="text" class="" id="conveyanceallowance" placeholder=""></td>
</tr>

<tr>
<td id="houserentcolor"><label class="" for="">House Rent </label></td>         
<td><input type="text" class="" id="houserentallowance" placeholder=""></td>
</tr>

<tr>
<td id="medicalcolor"><label class="" for="">Medical </label></td>       
<td><input type="text" class="" id="medicalallowance" placeholder=""></td>
</tr>


</tbody>
</table>

</div>

</div>


<div class="container">


<div class="col-md-6 form-horizontal" >



<input type="button" value="Add" class="plusbtn" />
<input type="button" value="Remove" class="minusbtn" />

<table class=" table-bordered" id="sizetable" >




<div id="additemdilauge">
<h3 id="itemdetails"><mark> Items Details </mark></h3>
</br>
<h4><mark>Name</mark></h4>
<input type="text" class="" id="" placeholder="">
</div>

<tr>
<th id="earning">Earnings
<button type="button" id="additempopup" class="btn btn-default">Add Items</button></th>
<th id="coloramountsize">Amount</th>
<th id="colorsizecompany">Company</th>
<th id="coloremployeesize">Employee</th>

       
</tr>

<tbody>

<tr>
    
<td><input type="text" class="textboxsize" id="earningsize" placeholder=""></td>
<td><input type="text" class="textboxsize" id="earamountsize" placeholder=""></td>
<td><input type="text" class="textboxsize" id="earcompanysize" placeholder=""></td>
<td><input type="text" class="textboxsize" id="earemployeesize" placeholder=""></td>

</tr>

</tbody>


</table>
</div>





<div class="col-md-6">


<input type="button" value="Add" class="dedplusbtn" />
<input type="button" value="Remove" class="dedminusbtn" />

<table class=" table-bordered" id="dedsizetable1" >

<tr>

<div id="deductionpopup">
<h3 id=""><mark> Items Details </mark></h3>
</br>
<h4><mark>Name</mark></h4>
<input type="text" class="" id="" placeholder="">
</div>

<th id="deduction" >Deduction
<button type="button" id="deductionaddpopup" class="btn btn-default">Add Items</button></th>
<th id="coloramount">Amount</th>
<th id="colorcompany">Company</th>
<th id="coloremp">Employee</th>

</tr>

<tbody>
<tr>
    
<td><input type="text" class="dedtextbox1" id="dedearning" placeholder=""></td>
<td><input type="text" class="dedtextbox1" id="dedamount" placeholder=""></td>
<td><input type="text" class="dedtextbox1" id="dedcompany" placeholder=""></td>
<td><input type="text" class="dedtextbox1" id="dedemployee" placeholder=""></td>

</tr>

</tbody>

</table>
</div>

</div>

<div class="container ">

<div class="col-md-6">

<div class="form-group ">
<label class="col-sm-5 control-label " for="lg">Total Earnings:</label>
<div class="col-sm-7">
<input class="" type="text" id="totalearning" placeholder="Enter TE">
</div>
</div>


</div>

<div class="col-md-6">

<label class="col-sm-5 control-label " for="lg">Total Deduction:</label>
<div class="col-sm-7">
<input class="" type="text" id="totaldeduction"  placeholder="Enter TD">
</div>


</div>

</div>

<div class="container">

<div class="col-md-6">

<div class="form-group ">
<label class="col-sm-5 control-label" for="lg">Previous Balance:</label>
<div class="col-sm-6">
<input class="" type="text" id="Previousbalance" placeholder="Enter PB">
</div>
</div>

<div class="form-group ">
<label class="col-sm-5 control-label" for="lg">Carry Over Round-Off:</label>
<div class="col-sm-7">
<input class="" type="text" id="CarryOverRoundOff" placeholder="Enter Carry Over R-Off">
</div>
</div>




</div>

<div class="col-md-6">


<label class="col-sm-5 control-label " for="lg">Net Pay(Rounded):</label>
<div class="col-sm-7">
<input class="" type="text" id="NetPayRounded" placeholder="Net Pay">
</div>


</div>

</div>

<div class="container">

<div class="col-md-9 form-group form-group-lg">
<h6>___________________________</h6>
<label class="control-label" for="">Employee's Signature</label>
</div>

<div class="col-md-3 form-group form-group-lg">
<h6>___________________________</h6>
<label class="control-label" for="">Employer's Signature</label>
</div>

</div>


<div class="container form-horizontal">
<textarea class="col-md-12" id="textareatext" style="resize:none">OneSquareSolutions</textarea>
</div>



<div class="container col-md-2">

<div class="form-group"> 
<button type="submit" id="payrollsubmit" value="" name="" class="form-control">Submit </button> 	  
</div>
</div>


</div>	
</div>

<div class="container col-md-12 form-horizontal">

<div class="panel panel-default">
<div class="panel-body">A Basic Panel</div>
</div>
</div>


<div class="container">
<footer id="footer">
<h6 id="footercolor">Copyrights © 2014 " Billing Soft ". All Rights Reserved.</h6>
<h6 id="footercolor">Designed by :<a href="http://www.onesquaresolutions.com/" id="foo1" target="_blank">One Square Software Solution</a></h6>
</footer>
</div>
	
</div>



</body>
</html>