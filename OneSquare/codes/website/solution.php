<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
  
   <style>
  
   
  
#b{
float:left;
height:8em;
width:20em;
}  
  
  #c
  {height:8em;
width:20em;
float:right;
}


   .carousel-inner > .item > img,
  .carousel-inner > .item > a > img {
      width: 50%;
      margin: auto;
	  height:20em;
	  float:left;
	  margin-top:5em;
	 } 
 
 #carousel
 {
 
 margin-top:-1.6%;
}

#paragraph
{
text-align:center;
width:100%;
height:100%;
color:#0000CC;
margin-top:1%;
font-family:"Times New Roman", Times, serif;
}
#paragraph:hover
{
color:#800000;
}

#para
{
font-family:"Times New Roman", Times, serif;
}
#para:hover
{
color:#800000;
}

#ser
{
float:none;
color:#1C44E3;
font-family:"Times New Roman", Times, serif;
text-decoration:none;
}


  
#head
{
text-align:center;
font-family:"Times New Roman", Times, serif;
color:#3333FF;
height:0em;
padding:0em;
margin:0em;
}
#head:hover
{color:#800000;
}


<!--------jspider --------------->
#footer{

background-color: #orange;
border-top: 4px solid #f78e21;
margin-top: 3px;
padding-top: 32px;

}
 
h4, .h4 {
font-size: 18px;
} 

.h4, .h5, .h6, h4, h5, h6 {
margin-top: -5px;
margin-bottom: 10px;
}

#pstyle
 {
font-family: 'Open Sans', sans-serif;
font-size: 13px;
color: #666;


display: block;
-webkit-margin-before: 1em;
-webkit-margin-after: 1em;
-webkit-margin-start: 0px;
-webkit-margin-end: 0px;
}
</style>

<!-----------------------------end of style------------------------------>

<body>
<div class="container" >
<h1 id="head"> One Square Solution </h1>
  <img src="1.jpg" alt="Cinque Terre"  id="b"></img> 
  <img src="4.jpg" alt="Cinque Terre" id="c"></img> 
</div>

<!----------------------------------------slide show begin--------------------------------->

<div class="container" id="carousel">
  <br>
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
      <li data-target="#myCarousel" data-slide-to="3"></li>
	  <li data-target="#myCarousel" data-slide-to="4"></li>
	  <li data-target="#myCarousel" data-slide-to="5"></li>
	  <li data-target="#myCarousel" data-slide-to="6"></li>
	  <li data-target="#myCarousel" data-slide-to="7"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">

      <div class="item active">
        <img src="images/28.jpg" />
		   <img src="images/29.jpg" />
        <div class="carousel-caption">
    
        </div>
      </div>

      <div class="item">
        <img src="images/30.jpg" />
		 <img src="images/31.jpg" />
        <div class="carousel-caption">
      
     </div>
      </div>
    
      <div class="item">
        <img src="images/32.jpg"  />
		 <img src="images/33.jpg"  />
        <div class="carousel-caption">
         
        </div>
      </div>


    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>

<div class="container_fluid">
<marquee width="100%" behavior="right" bgcolor="orange" loop="15">Are you dreaming to become a Software Test Engineer? <b><a href="branches">contact us for demo classes</a></b></marquee>
</div>
<!---------------------------------end of slide show-------------------->

<div class="container">
<h1 id="paragraph" >Welcome To One Square Solution </h1>
 <div class="panel panel-default">
<div class="panel-body" id="para" >
<h1 id="ser">"Solutions"</h1>
One Square Solutions is a single platform for all your system hardware and IT requirements. We have the expertise and experience in providing customized solutions for organizations in different sectors. Our hardware solutions and maintenance services are considered to be among the best in the whole country. We offer complete end-to-end hardware solutions from architecture and procurement to installation, testing and maintenance.</br>
We offer onsite and offsite service, our onsite services include deployment of the best available talented service engineers. Our range of solutions includes server installation and maintenance, our expertise in this field lies in Windows servers, Linux servers and Solaris.</br>
Other solutions offered by us include Router Firewall installation and maintenance of Cisco router and firewall, D-Link router and firewall, and Net Gear router and firewall; data backup which includes tape backup, data backup on disk and remote data backup; different kinds of printer maintenance, network deployment, PC hardware repair and upgrade, software installation, small application development, all kinds of mother boards and logic boards, disaster recovery, wireless network installation, and Virus, Spyware and Trojan removal.</br>

</div>
</div>
</div>

<!-------------------------------------------------end of services-------------->

<div class="container">
<div class="row">
<footer id="footer">


<div class="col-md-4">
<h4 class="footer-title">About JSpiders</h4>
<p id="pstyle">JSpiders is No.1 JAVA/J2EE training institute in India with a view to bridge the gap between Industry Requirement
 and curriculum of educational institutions and also to meet the ever increasing demand for Quality IT professional.
</p>

</div>


<div class="row">
<div class="col-md-4">
<a name="call"></a>
<h4 class="footer-title">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Contact Information
</h4>
<ul>
✆ Basavanagudi:
<br>	
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 9686114422 / 9686995511
<br>
									                                        
✆ Old Airport Road: 
<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 9686114422 / 9686995511
<br>

<p><span class="glyphicon glyphicon-envelope"> shashidharven@gmail.com</span></p>    

</ul>
										
</div>



<div class="col-md-3">
<h4 class="">
More info
</h4>
<p id="pstyle">We have multifarious
 activities encompassing java training, Placements, Deployment of profecessionals and technology research.</p>
 

</div>

</div>
</footer>
</div>
</div>

<div class="container_fluid">

<div class="bottom-footer">
        <div style="text-align:center;padding: 10px 0 10px 0; background: ;">
          <p>
            Copyright © 2014 
            <a href="#">
              Jspiders
            </a>
          </p>
        </div>
      </div>



</div>	

</body>
</html>
