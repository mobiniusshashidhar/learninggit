<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
  <script type="text/javascript" src="http://code.jquery.com/jquery-1.9.1.js"></script>
  <script type="text/javascript" src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
  <link rel="stylesheet" href="/resources/demos/style.css" />

    <script type="text/javascript">
        $(function () {

            var categories = ["Coke", "Pepsi", "Cake", "Buns", "Cheese", "Curd"];

            $('#txt_products').autocomplete({
                minLength: 0,
                source: categories,
                focus: function (event, ui) {
                    $("#txt_products").val(ui.item.label);
                    return false;
                },
                select: function (event, ui) {
                    if (ui.item.label == "Coke" || ui.item.label == "Pepsi")
                        $("#txt_category").val("Beverages");
                    else if (ui.item.label == "Cake" || ui.item.label == "Buns")
                        $("#txt_category").val("Condiments");
                    else if (ui.item.label == "Cheese" || ui.item.label == "Curd")
                        $("#txt_category").val("Diary");
                }
            });

        });

   
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
   Product:&nbsp;&nbsp;  <input id="txt_products" type="text" /> &nbsp;&nbsp;
      Category:&nbsp;&nbsp;  <input id="txt_category" type="text" />

    </div>
    </form>
</body>
</html>