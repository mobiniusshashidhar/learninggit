
<?php

include_once 'config.php' ;


session_start() ; // Starting Session
$error=''; // Variable To Store Error Message
if (isset($_POST['submit'])) 
{
if (empty($_POST['username']) || empty($_POST['password'])) 
{
$error1 = "Enter Correct Username or Password";
echo $error1 ;
}
else
{
// Define $username and $password

$username=$_POST['username'];
$password=$_POST['password'];
// To protect MySQL injection for Security purpose
$username = stripslashes($username);
$password = stripslashes($password);

// Establishing Connection with Server by passing server_name, user_id and password as a parameter

include_once 'config.php' ;

$username = mysql_real_escape_string($username);
$password = mysql_real_escape_string($password);

$query = mysql_query("select * from `login` where password='".$password."' AND username='".$username."'", $connection);
$rows = mysql_num_rows($query);
if ($rows == 1) {
$_SESSION['login_user']=$username; // Initializing Session
       header("Location: index.php"); // Redirecting To Other Page
} else {
echo '<script>';
echo 'alert("Username or Password is invalid");';
echo '</script>';
} 
mysql_close($connection); // Closing Connection
}
}
?> 


<!DOCTYPE html>
<html lang="en">
<title>Popup Login and Register</title>
<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="js/jquery.leanModal.min.js"></script>
<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" />
<link type="text/css" rel="stylesheet" href="css/style.css" />

<body>
<div class="container">
<form action="" method="post">
	<a id="modal_trigger" href="#modal" class="btn">Click here to Login or register</a>

	<div id="modal" class="popupContainer" style="display:none;">
		<header class="popupHeader">
			<span class="header_title">Login</span>
			<span class="modal_close"><i class="fa fa-times"></i></span>
		</header>
		
		<section class="popupBody">
			<!-- Social Login -->
			<div class="social_login">
				<!---<div class="">
					<a href="#" class="social_box fb">
						<span class="icon"><i class="fa fa-facebook"></i></span>
						<span class="icon_title">Connect with Facebook</span>
						
					</a>

					<a href="#" class="social_box google">
						<span class="icon"><i class="fa fa-google-plus"></i></span>
						<span class="icon_title">Connect with Google</span>
					</a>
				</div>---->

				<div class="centeredText">
					<span>use your Email address</span>
				</div>

				<div class="action_btns">
					<div class="one_half"><a href="#" id="login_form" class="btn">Login</a></div>
					<div class="one_half last"><a href="#" id="register_form" class="btn">Sign up</a></div>
				</div>
			</div>

			<!-- Username & Password Login form -->
			<div class="user_login">
	
					<label>Email / Username</label>
					<input type="text" name="username" required/>
					<br />

					<label>Password</label>
					<input type="password" name="password" required/>
					<br />

					<!--<div class="checkbox">
						<input id="remember" type="checkbox" />
						<label for="remember">Remember me on this computer</label>
					</div>--->

					<div class="action_btns">
						<div class="one_half"><a href="#" class="btn back_btn"><i class="fa fa-angle-double-left"></i> Back</a></div>
						<div class="one_half last"><button type="submit" href="#" name="submit" class="btn btn_red">Login</button></div>
					</div>
					<span><?php echo $error; ?></span>
			

				<!--<a href="#" class="forgot_password">Forgot password?</a>-->
			</div>

			<!-- Register Form -->
			<div id="mydivview">
			</div>
			<div class="user_register">
				
				

					<label>Email Address</label>
					<input type="email" id="regname"/>
					<br />

					<label>Password</label>
					<input type="password" id="regpassword" />
					<br />

					<!--<div class="checkbox">
						<input id="send_updates" type="checkbox" />
						<label for="send_updates">Send me occasional email updates</label>
					</div>--->

					<div class="action_btns">
						<div class="one_half"><a href="#" class="btn back_btn"><i class="fa fa-angle-double-left"></i> Back</a></div>
						<div class="one_half last"><button type="button" id="regsubmit" href="#" class="btn btn_red">Register</button></div>
					</div>
				
			</div>
		</section>
	</div>
	</form>
</div>

<script type="text/javascript">
	$("#modal_trigger").leanModal({top : 200, overlay : 0.6, closeButton: ".modal_close" });

	$(function(){
		// Calling Login Form
		$("#login_form").click(function(){
			$(".social_login").hide();
			$(".user_login").show();
			return false;
		});

		// Calling Register Form
		$("#register_form").click(function(){
			$(".social_login").hide();
			$(".user_register").show();
			$(".header_title").text('Register');
			return false;
		});

		// Going back to Social Forms
		$(".back_btn").click(function(){
			$(".user_login").hide();
			$(".user_register").hide();
			$(".social_login").show();
			$(".header_title").text('Login');
			return false;
		});

	})
</script>
<script>
$(document).ready(function() {

        $("#regsubmit").click(function() { 
						var regname=$("#regname").val();
						var regpassword=$("#regpassword").val();
					 
                      
                          $.ajax({
                              type:"post",
                              url:"ajaxpage.php",
          
							   data:"empname="+regname+"&emppass="+regpassword,
                              success:function(data){
                                 $("#mydivview").html(data);
                              }
 
                          });
        });
		    });
</script>

</body>
</html>
