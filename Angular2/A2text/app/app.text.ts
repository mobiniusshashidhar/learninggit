import { Component } from '@angular/core';

@Component ({
selector : 'my-app1',
templateUrl:`app/appservice.html`,
styleUrls:['app/style.css']
})

export class text {
title:string;
para:string;

constructor () {
this.title = "Hi Angular 2.o";
this.para = "What is the need of TypeScript  As you all know that JavaScript is everywhere and it is the only way to write a cross-platform application today. JavaScript was intended to write small application as it lacks static typing. The more mature languages like C#, Java etc. we use key concepts like classes, modules and interfaces";

}

}

@Component({
  selector: 'relative-path',
  templateUrl: 'app/component.html',
  styleUrls:['app/style.css']
})

export class SomeRelativeComponent {

title1:string;
para1:string;

constructor () {
this.title1 = "Hi Angular 2.o";
this.para1 = " As you all know that JavaScript is everywhere and it is the only way to write a cross-platform application today. JavaScript was intended to write small application as it lacks static typing. The more mature languages like C#, Java etc. we use key concepts like classes, modules and interfaces";

}
}

@Component ({
selector:'mytext',
templateUrl:`app/addingfields.html`
})

export class addingRows {

languages : any;

constructor () {
this.languages = ['Angularljs 1.5.6','Angularljs 2.0','TypeScript','Javascript'];
}

addrow(value: any) {

this.value = undefined;
if (value ) {
 this.languages.push(value);
}

 }


}
