import { Component } from '@angular/core';
import {text,SomeRelativeComponent,addingRows} from './app.text';


@Component ({
selector : 'my-app',
template : `<h1>Welcome To Angular 2.0</h1>
<my-app1></my-app1>
<relative-path></relative-path>
<mytext></mytext> `,


styles: [`
h1 {
text-align:center;
font-size:60px;
text-shadow:10px 5px 0 0 ;
}
`],

 directives:[text,SomeRelativeComponent,addingRows]
})
export class AppComponent {}
