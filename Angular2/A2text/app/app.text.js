"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var text = (function () {
    function text() {
        this.title = "Hi Angular 2.o";
        this.para = "What is the need of TypeScript  As you all know that JavaScript is everywhere and it is the only way to write a cross-platform application today. JavaScript was intended to write small application as it lacks static typing. The more mature languages like C#, Java etc. we use key concepts like classes, modules and interfaces";
    }
    text = __decorate([
        core_1.Component({
            selector: 'my-app1',
            templateUrl: "app/appservice.html",
            styleUrls: ['app/style.css']
        }), 
        __metadata('design:paramtypes', [])
    ], text);
    return text;
}());
exports.text = text;
var SomeRelativeComponent = (function () {
    function SomeRelativeComponent() {
        this.title1 = "Hi Angular 2.o";
        this.para1 = " As you all know that JavaScript is everywhere and it is the only way to write a cross-platform application today. JavaScript was intended to write small application as it lacks static typing. The more mature languages like C#, Java etc. we use key concepts like classes, modules and interfaces";
    }
    SomeRelativeComponent = __decorate([
        core_1.Component({
            selector: 'relative-path',
            templateUrl: 'app/component.html',
            styleUrls: ['app/style.css']
        }), 
        __metadata('design:paramtypes', [])
    ], SomeRelativeComponent);
    return SomeRelativeComponent;
}());
exports.SomeRelativeComponent = SomeRelativeComponent;
var addingRows = (function () {
    function addingRows() {
        this.languages = ['Angularljs 1.5.6', 'Angularljs 2.0', 'TypeScript', 'Javascript'];
    }
    addingRows.prototype.addrow = function (value) {
        this.value = undefined;
        if (value) {
            this.languages.push(value);
        }
    };
    addingRows = __decorate([
        core_1.Component({
            selector: 'mytext',
            templateUrl: "app/addingfields.html"
        }), 
        __metadata('design:paramtypes', [])
    ], addingRows);
    return addingRows;
}());
exports.addingRows = addingRows;
//# sourceMappingURL=app.text.js.map