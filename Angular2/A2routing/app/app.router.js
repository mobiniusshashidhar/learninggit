"use strict";
var router_1 = require('@angular/router');
var christlist_1 = require('./christlist');
var hereoslist_1 = require('./hereoslist');
exports.routes = [
    { path: 'crisis-center', component: christlist_1.christListComponent },
    { path: 'heroes', component: hereoslist_1.hereostListComponent }
];
exports.APP_ROUTER_PROVIDERS = [
    router_1.provideRouter(exports.routes)
];
//# sourceMappingURL=app.router.js.map