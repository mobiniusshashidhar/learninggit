"use strict";
var router_1 = require("@angular/router");
var crisis_list_component_1 = require('./crisis-list.component');
var hero_list_component_1 = require('./hero-list.component');
exports.routes = [
    { path: 'crisis-center', component: crisis_list_component_1.CrisisListComponent },
    { path: 'heroes', component: hero_list_component_1.HeroListComponent }
];
exports.APP_ROUTER_PROVIDERS = [
    router_1.provideRouter(exports.routes)
];
//# sourceMappingURL=app.routes.js.map