import { Directive, ElementRef , HostListener, Input } from '@angular/core';

@Directive({ selector :'[myHighlight]'})

export class HighlightDirective {
private _defaultColor = 'red';
private ele : HTMLElement;

constructor (ele:ElementRef) {
this.ele = ele.nativeElement; }


@Input('myHighlight') highlightColor: string;

@HostListener ('mouseenter') onMouseEnter ( ) {
this.highlight(this.highlightColor || this._defaultColor);
}
@HostListener ('mouseleave') onMouseLeave ( ) {
this.highlight(null);
}

private highlight (color :string) {
this.ele.style.backgroundColor = color;
}

}
