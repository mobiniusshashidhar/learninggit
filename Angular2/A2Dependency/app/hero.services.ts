import { Injectable } from '@angular/core';
import { HEROES }     from './mock-heroes';

export class HeroService {
  getHeroes() { return HEROES;  }
}
