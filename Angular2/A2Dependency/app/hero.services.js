"use strict";
var mock_heroes_1 = require('./mock-heroes');
var HeroService = (function () {
    function HeroService() {
    }
    HeroService.prototype.getHeroes = function () { return mock_heroes_1.HEROES; };
    return HeroService;
}());
exports.HeroService = HeroService;
//# sourceMappingURL=hero.services.js.map