import { Component } from '@angular/core';
import { HeroService }  from './hero.services';
import { HeroListComponent }  from './hero-listcomponent';

@Component ({
selector : 'my-heroes',
template : `<h2>Heroes</h2>
  <hero-list></hero-list>
`,
 directives: [HeroListComponent]
})

export class HeroesComponent { }
