import { Component } from '@angular/core';

@Component {
selector : 'addingrow',
template : `
<p>{{title}}</p>
<ul>
<ol *ngFor="let lang of languages ">
{{lang}}
</ol>
</ul> `

}
export class addingRows {
title:string;
languages:string;

constructor () {
this.title = 'Angularjs 2.0';
this.languages = ['Angularljs 1.0','Angularljs 2.0','Javascript','Javascript'];
}

}
