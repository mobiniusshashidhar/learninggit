import { Component } from '@angular/core';

@Component ({
selector : 'my-app',
template : `
    <h1 >{{title}}</h1>
<ul>
<li *ngFor="let vers of versions">
{{ vers }}
</li>
</ul>
`
})

export class AppComponent {
title:string;
versions:string;

constructor () {
this.title = 'Angular 2.0';
this.versions = ['Es5','Es6','mocha','livescript','typescript'];
}

}
