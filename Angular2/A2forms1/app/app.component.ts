
import { Component } from '@angular/core';
import { REACTIVE_FORM_DIRECTIVES, FormBuilder, Validators } from '@angular/forms';
import { ValidationService } from 'app/validationservice';

@Component({
  selector: 'demo-app',
  templateUrl: 'app/app.component.html',
  directives: [REACTIVE_FORM_DIRECTIVES]
})
export class AppComponent {
  userForm: any;

  constructor(private _formBuilder: FormBuilder) {

  this.userForm = this.formBuilder.group({
      'name': ['', Validators.required],
      'email': ['', [Validators.required, ValidationService.emailValidator]],
      'profile': ['', [Validators.required, Validators.minLength(10)]]
    });
  }


  saveUser() {
    if (this.userForm.dirty && this.userForm.valid) {
      alert(`Name: ${this.userForm.value.name} Email: ${this.userForm.value.email}`);
    }
  }
}
