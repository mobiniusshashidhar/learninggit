import { Component } from '@angular/core';

@Component ({
selector : 'my-app',
template : `<h1>Pipes</h1>

<p>The hero's birthday is {{  birthday | date:format1 : 'fullDate' | uppercase}} </p>
<button type="button" (click) = "toggleFormate()">Toggle</button>
`
})

export class heroBirhdayComponent {

birthday = new Date( ); // April 15, 1988
toggle = true; // start with true == shortDate


get format1() {return this.toggle ? 'shortDate':'fullDate';}
toggleFormate() { this.toggle = !this.toggle; }

 }
