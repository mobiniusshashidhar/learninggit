"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var heroBirhdayComponent = (function () {
    function heroBirhdayComponent() {
        this.birthday = new Date(); // April 15, 1988
        this.toggle = true; // start with true == shortDate
    }
    Object.defineProperty(heroBirhdayComponent.prototype, "format1", {
        get: function () { return this.toggle ? 'shortDate' : 'fullDate'; },
        enumerable: true,
        configurable: true
    });
    heroBirhdayComponent.prototype.toggleFormate = function () { this.toggle = !this.toggle; };
    heroBirhdayComponent = __decorate([
        core_1.Component({
            selector: 'my-app',
            template: "<h1>Pipes</h1>\n\n<p>The hero's birthday is {{  birthday | date:format1 : 'fullDate' | uppercase}} </p>\n<button type=\"button\" (click) = \"toggleFormate()\">Toggle</button>\n"
        }), 
        __metadata('design:paramtypes', [])
    ], heroBirhdayComponent);
    return heroBirhdayComponent;
}());
exports.heroBirhdayComponent = heroBirhdayComponent;
//# sourceMappingURL=app.component.js.map