"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var addingRows = (function () {
    function addingRows() {
        this.title = 'Angularjs 2.0';
        this.languages = ['Angularljs 1.0', 'Angularljs 2.0', 'Javascript', 'Javascript'];
    }
    addingRows.prototype.addrow = function (box) {
        if (box) {
            this.languages.push(box);
        }
    };
    addingRows = __decorate([
        core_1.Component({
            selector: 'addingrow',
            template: "\n<p>{{title}}</p>\n<input #box (keyup.enter) = \"addrow(box.value)\" (blur)=\"addrow(box.value); box.value='' \" >\n\n<button (click) = \"addrow(box.value)\">Add</button><br/>\n\n<ol>\n<li *ngFor=\"let lang of languages \">\n{{lang}}\n</li>\n</ol> "
        }), 
        __metadata('design:paramtypes', [])
    ], addingRows);
    return addingRows;
}());
exports.addingRows = addingRows;
//# sourceMappingURL=addrow.js.map