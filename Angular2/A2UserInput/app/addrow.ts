import { Component } from '@angular/core';

@Component ({
selector : 'addingrow',
template : `
<p>{{title}}</p>
<input #box (keyup.enter) = "addrow(box.value)" (blur)="addrow(box.value); box.value='' " >

<button (click) = "addrow(box.value)">Add</button><br/>

<ol>
<li *ngFor="let lang of languages ">
{{lang}}
</li>
</ol> `

})

export class addingRows {

title:any;
languages : any;

addrow(box: string) {
 if (box) {
    this.languages.push(box);
 }
}
constructor () {
this.title = 'Angularjs 2.0';
this.languages = ['Angularljs 1.0','Angularljs 2.0','Javascript','Javascript'];
}

}
