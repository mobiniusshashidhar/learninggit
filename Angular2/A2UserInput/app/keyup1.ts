import { Component } from '@angular/core';

@Component ({
selector : 'keyup1',
template : `
<input #box (keyup) = "onKey(box.value)">
<p>{{values}}</p>
`
})

export class loopbackApp1 {
values = ' ';
onKey(value:string) {
this.values+=value+'|';
}
}
